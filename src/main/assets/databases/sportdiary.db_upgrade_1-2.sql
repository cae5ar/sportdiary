CREATE TABLE IF NOT EXISTS "BodyCharacteristics" (
  "measurementDate" integer PRIMARY KEY NOT NULL,
  "chestValue" real NOT NULL,
  "bicepsValue" real NOT NULL,
  "waistValue" real NOT NULL,
  "hipsValue" real NOT NULL,
  "totalWeightValue" real NOT NULL,
  "muscleWeightValue" real NOT NULL,
  "fatPercentValue" real NOT NULL
);