package com.kasimov.sport_diary2.fragment;

public interface CanManageExercises {
    public void notifyDataSetChanged();
    public void loadShow(boolean asList);
}
