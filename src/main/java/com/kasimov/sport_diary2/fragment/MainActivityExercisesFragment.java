package com.kasimov.sport_diary2.fragment;

import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.kasimov.sport_diary2.R;
import com.kasimov.sport_diary2.activity.MainActivity;
import com.kasimov.sport_diary2.adapter.ExercisesListAdapter;
import com.kasimov.sport_diary2.adapter.MuscleGroupRecyclerAdapter;
import com.kasimov.sport_diary2.model.ExerciseGroup;
import com.kasimov.sport_diary2.model.ExerciseType;
import com.kasimov.sport_diary2.util.SimpleCallback;

import java.util.Arrays;

public class MainActivityExercisesFragment extends AbstractTabFragment implements CanManageExercises {

    private SimpleCallback clickCallback;
    private SimpleCallback removeCallback;
    private ExercisesListAdapter.CheckDelegate checker;
    private ExercisesListAdapter adapter;
    private RecyclerView recyclerView;
    private ListView listView;
    private MuscleGroupRecyclerAdapter muscleGroupRecyclerAdapter;
    private boolean viewMode = true;


    public MainActivityExercisesFragment() {
        setTitle("Упражнения");
    }

    public static MainActivityExercisesFragment newInstance() {
        return MainActivityExercisesFragment.newInstance(null, null, null);
    }

    public static MainActivityExercisesFragment newInstance(SimpleCallback clickCallback, SimpleCallback removeCallback, ExercisesListAdapter.CheckDelegate checker) {
        MainActivityExercisesFragment fragment = new MainActivityExercisesFragment();
        fragment.setClickCallback(clickCallback);
        fragment.setRemoveCallback(removeCallback);
        fragment.setChecker(checker);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(getActivity() instanceof MainActivity)
            getMainActivity().setActivityExercisesFragment(this);
        View view = inflater.inflate(R.layout.exercise_list_fragment, container, false);
        listView = (ListView) view.findViewById(R.id.main_exercise_list);
        listView.setDivider(null);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        loadShow(viewMode);
        return view;
    }

    public void loadShow(boolean asList) {
        if (asList) {
            adapter = new ExercisesListAdapter(getActivity(), clickCallback, removeCallback, checker, ExerciseType.valuesSorted());
            listView.setAdapter(adapter);
            listView.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        } else {
            muscleGroupRecyclerAdapter = new MuscleGroupRecyclerAdapter(clickCallback, removeCallback, checker, Arrays.asList(ExerciseGroup.values()));
            recyclerView.setAdapter(muscleGroupRecyclerAdapter);
            recyclerView.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
        }
    }

    public void setClickCallback(SimpleCallback clickCallback) {
        this.clickCallback = clickCallback;
    }

    public void setRemoveCallback(SimpleCallback removeCallback) {
        this.removeCallback = removeCallback;
    }

    public void setChecker(ExercisesListAdapter.CheckDelegate checker) {
        this.checker = checker;
    }

    public void notifyDataSetChanged() {
        if (muscleGroupRecyclerAdapter != null)
            muscleGroupRecyclerAdapter.notifyDataSetChanged();
        if (adapter != null)
            adapter.notifyDataSetChanged();
    }


    public void setViewMode(boolean viewMode) {
        this.viewMode = viewMode;
    }
}
