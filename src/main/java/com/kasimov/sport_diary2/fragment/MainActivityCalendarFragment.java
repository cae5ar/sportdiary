package com.kasimov.sport_diary2.fragment;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CalendarView;
import android.widget.ListView;
import android.widget.TextView;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.kasimov.sport_diary2.R;
import com.kasimov.sport_diary2.adapter.BelowCalendarExercisesAdapter;
import com.kasimov.sport_diary2.model.Exercise;
import com.kasimov.sport_diary2.model.Training;
import com.kasimov.sport_diary2.util.DateUtils;
import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

import org.androidannotations.annotations.EFragment;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@EFragment
public class MainActivityCalendarFragment extends AbstractTabFragment {

    private static final int EDIT_REQUEST_CODE = 4;
    private static MainActivityCalendarFragment instance;
    private CalendarView calendar;

    private ListView exercisesListView;
    private TextView trainingTypeCapiton;
    private FloatingActionButton addActionButton;
    private CaldroidFragment caldroidFragment;
    private SimpleDateFormat yyyyMMddFormat;

    private Drawable background;
    private Date prevDate = new Date(0);
    private View clickedDate = null;
    private FloatingActionMenu menuTrainingActions;

    public static MainActivityCalendarFragment getInstance() {
        return MainActivityCalendarFragment_.builder().build();
    }

    public MainActivityCalendarFragment() {
        setTitle("Календарь");
    }

    @Override
    public void onStart() {
        super.onStart();
        Calendar calendarInstance = DateUtils.trimTime(new Date().getTime());
        getMainActivity().loadTrainingForSelectedDate(calendarInstance.getTimeInMillis());
        getMainActivity().loadTrainingDates();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getMainActivity().setActivityCalendarFragment(this);
        View layout = inflater.inflate(R.layout.fragment_main_activity_calendar, null);
        trainingTypeCapiton = (TextView) layout.findViewById(R.id.trainingTypeCapiton);
        yyyyMMddFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

        caldroidFragment = new CaldroidFragment();
        Bundle args = new Bundle();
        args.putInt(CaldroidFragment.START_DAY_OF_WEEK, CaldroidFragment.MONDAY);
        args.putBoolean(CaldroidFragment.SQUARE_TEXT_VIEW_CELL, true);
        args.putInt(CaldroidFragment.THEME_RESOURCE, R.style.CaldroidCustom);
        caldroidFragment.setArguments(args);

        FragmentManager supportFragmentManager = getMainActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = supportFragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.calendar_container, caldroidFragment);
        fragmentTransaction.disallowAddToBackStack();
        fragmentTransaction.commitAllowingStateLoss();

        caldroidFragment.setCaldroidListener(new CaldroidListener() {
            @Override
            public void onSelectDate(Date date, View view) {
                setClickedDate(date, view);
            }
        });
        addActionButton = (FloatingActionButton) layout.findViewById(R.id.addTrainingBtn);
        menuTrainingActions = (FloatingActionMenu) layout.findViewById(R.id.menuTrainingActions);
        layout.findViewById(R.id.editTraining).setOnClickListener(getMainActivity());
        layout.findViewById(R.id.removeTraining).setOnClickListener(getMainActivity());
        addActionButton.setOnClickListener(getMainActivity());
        exercisesListView = (ListView) layout.findViewById(R.id.mainTrainsList);
        return layout;
    }


    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    public void highlightTrainingDates(List<Date> trainingDates) {
        caldroidFragment.clearSelectedDates();
        for (Date date : trainingDates) {
            caldroidFragment.setSelectedDate(date);
        }
        caldroidFragment.refreshView();
    }

    public void updateExercisesBlock(Training trainingForSelectedDate) {
        Exercise[] objects = new Exercise[0];
        trainingTypeCapiton.setVisibility(View.INVISIBLE);
        if (!trainingForSelectedDate.getExercises().isEmpty()) {
            objects = new Exercise[trainingForSelectedDate.getExercises().size()];
            trainingForSelectedDate.getExercises().values().toArray(objects);
            trainingTypeCapiton.setText(trainingForSelectedDate.getType().title);
            trainingTypeCapiton.setVisibility(View.VISIBLE);
        }
        if (trainingForSelectedDate.getId() != null) {
            menuTrainingActions.setVisibility(View.VISIBLE);
            addActionButton.setVisibility(View.INVISIBLE);
        } else {
            menuTrainingActions.close(false);
            menuTrainingActions.setVisibility(View.INVISIBLE);
            addActionButton.setVisibility(View.VISIBLE);
        }
        BelowCalendarExercisesAdapter adapter = new BelowCalendarExercisesAdapter(getActivity(), objects);
        exercisesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Exercise item = (Exercise) adapterView.getItemAtPosition(i);
                getMainActivity().editExerciseResult(item);
            }
        });
        exercisesListView.setAdapter(adapter);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected void setClickedDate(Date date, View view) {
        if (date.equals(prevDate))
            return;
//        menuTrainingActions.close(true);
        prevDate = date;
        Calendar instance = DateUtils.trimTime(date);
        if (clickedDate != null) {
            clickedDate.setBackground(background);
        }
        loadTraininig(view, instance);
        clickedDate = view;
    }


    protected void loadTraininig(View view, Calendar instance) {
        background = view.getBackground();
        view.setBackgroundResource(R.drawable.clicked_cell_bg);
        getMainActivity().loadTrainingForSelectedDate(instance.getTimeInMillis());
    }

    @Override
    public void onStop() {
        super.onStop();
        prevDate = null;
    }
}