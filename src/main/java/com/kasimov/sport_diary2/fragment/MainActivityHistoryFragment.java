package com.kasimov.sport_diary2.fragment;


import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.kasimov.sport_diary2.R;
import com.kasimov.sport_diary2.adapter.TrainingCursorAdapter;
import com.kasimov.sport_diary2.util.AppDatabase;

public class MainActivityHistoryFragment extends AbstractTabFragment {

    private static MainActivityHistoryFragment instance;

    public MainActivityHistoryFragment() {
        setTitle("История");
    }

    public static MainActivityHistoryFragment getInstance() {
        if (instance == null)
            instance = new MainActivityHistoryFragment();
        return instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_activity_history, container, false);
        ListView historyListView = (ListView) view.findViewById(R.id.historyListView);
        AppDatabase instance = AppDatabase.getInstance(getActivity());
        Cursor trainingHistory = instance.getTrainingHistory();
        TrainingCursorAdapter trainingCursorAdapter = new TrainingCursorAdapter(getActivity(), trainingHistory);
        historyListView.setAdapter(trainingCursorAdapter);
        historyListView.setDivider(null);
        return view;
    }

}
