package com.kasimov.sport_diary2.fragment;


import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.kasimov.sport_diary2.R;
import com.kasimov.sport_diary2.model.Exercise;
import com.kasimov.sport_diary2.model.WeightUnitType;
import com.kasimov.sport_diary2.util.ValueParser;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

@EFragment(R.layout.fragment_edit_muscle_exercise)
public class EditMuscleExerciseFragment extends AbstractEditExerciseFragment implements View.OnClickListener {

    @ViewById
    protected EditText repetitionMin;
    @ViewById
    protected EditText repetitionMax;
    @ViewById
    protected EditText numberOfApproaches;
    @ViewById
    protected EditText workWeight;
    @ViewById
    protected RadioGroup weightUnitSelector;
    @ViewById
    protected RadioButton killogramSelector;
    @ViewById
    protected RadioButton poundsSelector;
    @ViewById
    protected RadioButton piecesSelector;

    public EditMuscleExerciseFragment() {
    }

    public static EditMuscleExerciseFragment getInstance(Exercise exercise) {
        EditMuscleExerciseFragment editMuscleExerciseFragment = new EditMuscleExerciseFragment_();
        editMuscleExerciseFragment.setExercise(exercise);
        return editMuscleExerciseFragment;
    }


    @AfterViews
    protected void updateView() {
        if (exercise.getWorkingWeight() != null) {
            workWeight.setText(exercise.getWorkingWeight() + "");
            switch (exercise.getWeightUnit()){
                case KILOGRAM:
                    killogramSelector.setChecked(true);
                    break;
                case POUNDS:
                    poundsSelector.setChecked(true);
                    break;
                case PIECES:
                    piecesSelector.setChecked(true);
                    break;
            }
        }
        if (exercise.getRepsMax() != null)
            repetitionMax.setText(exercise.getRepsMax() + "");
        if (exercise.getRepsMin() != null)
            repetitionMin.setText(exercise.getRepsMin() + "");
        if (exercise.getApproachesNumber() != null)
            numberOfApproaches.setText(exercise.getApproachesNumber() + "");
    }

    public Exercise getExercise(){
        exercise.setWorkingWeight(ValueParser.parseFloat(workWeight));
        exercise.setApproachesNumber(ValueParser.parseInt(numberOfApproaches));
        exercise.setRepsMax(ValueParser.parseInt(repetitionMax));
        exercise.setRepsMin(ValueParser.parseInt(repetitionMin));
        switch (weightUnitSelector.getCheckedRadioButtonId()){
            case R.id.killogramSelector:
                exercise.setWeightUnit(WeightUnitType.KILOGRAM);
                break;
            case R.id.poundsSelector:
                exercise.setWeightUnit(WeightUnitType.POUNDS);
                break;
            case R.id.piecesSelector:
                exercise.setWeightUnit(WeightUnitType.PIECES);
                break;
        }
        return exercise;
    }

    @Click({R.id.btn_add,R.id.btn_minus})
    public void onClick(View view) {
        int parseInt = ValueParser.parseInt(numberOfApproaches);
        switch(view.getId()){
            case R.id.btn_minus:
                parseInt = parseInt > 0 ? parseInt - 1 : parseInt;
                break;
            case R.id.btn_add:
                parseInt++;
                break;
        }
        numberOfApproaches.setText((parseInt)+"");
    }

}