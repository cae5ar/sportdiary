package com.kasimov.sport_diary2.fragment;


import android.support.v4.app.Fragment;

import com.kasimov.sport_diary2.model.Exercise;

public abstract class AbstractEditExerciseFragment extends Fragment{

    protected Exercise exercise;

    public void setExercise(Exercise exercise) {
        this.exercise = exercise;
    }

    public abstract Exercise getExercise();
}