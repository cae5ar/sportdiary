package com.kasimov.sport_diary2.fragment;

import android.support.v4.app.Fragment;

import com.kasimov.sport_diary2.activity.MainActivity;

public class AbstractTabFragment extends Fragment {

    protected String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    protected MainActivity getMainActivity() {
        return (MainActivity) getActivity();
    }
}
