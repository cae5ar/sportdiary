package com.kasimov.sport_diary2.fragment;


import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.kasimov.sport_diary2.R;
import com.kasimov.sport_diary2.model.DistanceUnitType;
import com.kasimov.sport_diary2.model.Exercise;
import com.kasimov.sport_diary2.util.ValueParser;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import info.hoang8f.android.segmented.SegmentedGroup;

@EFragment(R.layout.fragment_result_aerobic_exercise)
public class ResultAeroExerciseFragment extends AbstractEditExerciseFragment {

    @ViewById
    protected EditText comment;
    @ViewById
    protected EditText delta;
    @ViewById
    protected TextView speedLabel;
    @ViewById
    protected TextView durationLabel;
    @ViewById
    protected TextView speedUnitLabel;
    @ViewById(R.id.minus)
    protected RadioButton minusRadioBtn;
    @ViewById
    protected EditText averageSpeed;
    @ViewById
    protected EditText averageDuration;
    @ViewById
    protected EditText heartRate;
    @ViewById
    protected EditText distance;
    @ViewById
    protected RadioGroup distanceSelector;
    @ViewById
    protected RadioButton kilometerUnit;
    @ViewById
    protected RadioButton mileUnit;
    @ViewById
    protected SegmentedGroup deltaGroup;

    public static ResultAeroExerciseFragment getInstance(Exercise exercise) {
        ResultAeroExerciseFragment resultAeroExerciseFragment = new ResultAeroExerciseFragment_();
        resultAeroExerciseFragment.setExercise(exercise);
        return resultAeroExerciseFragment;
    }

    public ResultAeroExerciseFragment() {
    }

    @AfterViews
    protected void updateView() {
        speedLabel.setText("Скорость: " + exercise.getSpeed() + " " + exercise.getSpeedUnit().title);
        durationLabel.setText("Длительность: " + exercise.getDuration() + " мин.");
        comment.setText(exercise.getComment());
        Float deltaValue = exercise.getDelta();
        if (deltaValue == null)
            deltaValue = 0F;
        if (deltaValue < 0) {
            deltaValue *= -1;
            minusRadioBtn.setChecked(true);
        }
        if (exercise.getAverageSpeed() != null)
            averageSpeed.setText(exercise.getAverageSpeed() + "");
        if (exercise.getAverageDuration() != null)
            averageDuration.setText(exercise.getAverageDuration() + "");
        if (exercise.getHeartRate() != null)
            heartRate.setText(exercise.getHeartRate() + "");
        if (exercise.getDistanceUnit() != null) {
            distance.setText(exercise.getDistance() + "");
            switch (exercise.getDistanceUnit()) {
                case KILOMETER:
                    kilometerUnit.setChecked(true);
                    break;
                case MILE:
                    mileUnit.setChecked(true);
                    break;
            }
        }

        delta.setText(deltaValue + "");
        speedUnitLabel.setText(exercise.getSpeedUnit().title);
    }

    @Override
    public Exercise getExercise() {
        exercise.setComment(comment.getText().toString());
        exercise.setAverageSpeed(ValueParser.parseFloat(averageSpeed));
        exercise.setAverageDuration(ValueParser.parseInt(averageDuration));
        exercise.setHeartRate(ValueParser.parseInt(heartRate));
        exercise.setDistance(ValueParser.parseFloat(distance));
        switch (distanceSelector.getCheckedRadioButtonId()) {
            case R.id.mileUnit:
                exercise.setDistanceUnit(DistanceUnitType.MILE);
                break;
            case R.id.kilometerUnit:
                exercise.setDistanceUnit(DistanceUnitType.KILOMETER);
                break;
        }
        float parseInt = ValueParser.parseFloat(delta);
        switch (deltaGroup.getCheckedRadioButtonId()) {
            case R.id.plus:
                exercise.setDelta(parseInt);
                break;
            case R.id.minus:
                exercise.setDelta(parseInt * (-1));
                break;
        }
        return exercise;
    }
}
