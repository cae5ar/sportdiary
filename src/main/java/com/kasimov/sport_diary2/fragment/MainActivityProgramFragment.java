package com.kasimov.sport_diary2.fragment;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kasimov.sport_diary2.R;

public class MainActivityProgramFragment extends AbstractTabFragment implements View.OnClickListener {

    private static MainActivityProgramFragment instance;

    public MainActivityProgramFragment() {
        setTitle("Программы");
    }

    public static MainActivityProgramFragment getInstance() {
        if (instance == null) {
            instance = new MainActivityProgramFragment();
        }
        return instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main_activity_program, container, false);
        rootView.findViewById(R.id.getProgrammBtn).setOnClickListener(this);
        return rootView;

    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.getProgrammBtn){
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.programmUri)));
            startActivity(browserIntent);
        }
    }
}
