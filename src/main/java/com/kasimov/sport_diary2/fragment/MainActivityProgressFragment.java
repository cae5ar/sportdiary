package com.kasimov.sport_diary2.fragment;


import android.database.Cursor;
import android.support.v4.content.ContextCompat;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.kasimov.sport_diary2.R;
import com.kasimov.sport_diary2.model.BodyCharacteristics;
import com.kasimov.sport_diary2.util.AppDatabase;
import com.kasimov.sport_diary2.util.DateUtils;
import com.kasimov.sport_diary2.util.ValueParser;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.text.DecimalFormat;
import java.util.Date;

@EFragment(R.layout.fragment_main_activity_progress)
public class MainActivityProgressFragment extends AbstractTabFragment {

    protected static MainActivityProgressFragment instance;
    protected final DecimalFormat decimalFormat;
    @ViewById
    protected EditText chestValue;
    @ViewById
    protected EditText bicepsValue;
    @ViewById
    protected EditText waistValue;
    @ViewById
    protected EditText hipsValue;
    @ViewById
    protected EditText totalWeightValue;
    @ViewById
    protected EditText muscleWeightValue;
    @ViewById
    protected EditText fatPercentValue;

    @ViewById
    protected TextView deltaChestValue;
    @ViewById
    protected TextView deltaBicepsValue;
    @ViewById
    protected TextView deltaWaistValue;
    @ViewById
    protected TextView deltaHipsValue;
    @ViewById
    protected TextView deltaTotalWeightValue;
    @ViewById
    protected TextView deltaMuscleWeightValue;
    @ViewById
    protected TextView deltaFatPercentValue;

    @ViewById
    protected TextView dateOfLastMeasurement;
    @ViewById
    protected ImageView progressNextArrow;
    @ViewById
    protected ImageView progressPrevArrow;

    protected BodyCharacteristics firstBodyCharacteristics;
    protected Cursor currentBodyCharacteristics;
    protected BodyCharacteristics currentBodyCharacteristicsValue;
    protected boolean hasValues;

    protected AppDatabase database;

    public MainActivityProgressFragment() {
        setTitle("Прогресс");
        decimalFormat = new DecimalFormat("#.##");
    }

    public static MainActivityProgressFragment getInstance() {
        return MainActivityProgressFragment_.builder().build();
    }

    @AfterViews
    protected void afterViews(){
        database = AppDatabase.getInstance(getActivity());
        dateOfLastMeasurement.setText(DateFormat.format("dd MMMM yyyy г.", new Date()));
        reloadBodyCharacteristics();
    }

    @Background
    protected void reloadBodyCharacteristics() {
        firstBodyCharacteristics = database.getFirstBodyCharacteristics(null);
        currentBodyCharacteristics = database.getLatestBodyCharacteristics();
        hasValues = currentBodyCharacteristics.moveToFirst();
        reloadBodyCharacteristicValue(currentBodyCharacteristics);
    }

    protected void reloadBodyCharacteristicValue(Cursor currentBodyCharacteristics) {
        currentBodyCharacteristicsValue = database.extractBodyCharacteristics(currentBodyCharacteristics);
        updateCurrentBodyCharacteristics();
    }

    @UiThread
    protected void updateCurrentBodyCharacteristics() {
        if (hasValues){
            progressNextArrow.setVisibility(currentBodyCharacteristics.isFirst() ? View.INVISIBLE : View.VISIBLE);
            progressPrevArrow.setVisibility(currentBodyCharacteristics.isLast() ? View.INVISIBLE : View.VISIBLE);
        }else{
            progressNextArrow.setVisibility(View.INVISIBLE);
            progressPrevArrow.setVisibility(View.INVISIBLE);
        }
        updateBodyCharacteristicFields(currentBodyCharacteristicsValue);
        countAndShowDifference(firstBodyCharacteristics, currentBodyCharacteristicsValue);
    }

    protected void countAndShowDifference(BodyCharacteristics firstBodyCharacteristics, BodyCharacteristics latestBodyCharacteristics) {
        clearDeltaFields();
        if (firstBodyCharacteristics != null && latestBodyCharacteristics != null && firstBodyCharacteristics.getMeasurementDate() != latestBodyCharacteristics.getMeasurementDate()) {
            updateDeltaFor(firstBodyCharacteristics.getChestValue(), latestBodyCharacteristics.getChestValue(), deltaChestValue);
            updateDeltaFor(firstBodyCharacteristics.getBicepsValue(), latestBodyCharacteristics.getBicepsValue(), deltaBicepsValue);
            updateDeltaFor(firstBodyCharacteristics.getWaistValue(), latestBodyCharacteristics.getWaistValue(), deltaWaistValue);
            updateDeltaFor(firstBodyCharacteristics.getHipsValue(), latestBodyCharacteristics.getHipsValue(), deltaHipsValue);
            updateDeltaFor(firstBodyCharacteristics.getTotalWeightValue(), latestBodyCharacteristics.getTotalWeightValue(), deltaTotalWeightValue);
            updateDeltaFor(firstBodyCharacteristics.getMuscleWeightValue(), latestBodyCharacteristics.getMuscleWeightValue(), deltaMuscleWeightValue);
            updateDeltaFor(firstBodyCharacteristics.getFatPercentValue(), latestBodyCharacteristics.getFatPercentValue(), deltaFatPercentValue);
        }
    }

    protected void updateDeltaFor(float valueOld, float valueNew, TextView deltaTextView) {
        float v;
        String s = "";
        if (valueNew > valueOld) {
            deltaTextView.setTextColor(ContextCompat.getColor(getContext(), R.color.colorDeltaPositive));
            v = (valueNew / valueOld * 100) - 100;
            s = "+";
        } else if (valueNew < valueOld) {
            deltaTextView.setTextColor(ContextCompat.getColor(getContext(), R.color.colorDeltaNegative));
            v = (valueOld / valueNew * 100) - 100;
            s = "-";

        } else {
            deltaTextView.setTextColor(ContextCompat.getColor(getContext(), R.color.colorDeltaZero));
            v = 0;
        }
        deltaTextView.setText(s + decimalFormat.format(v) + "%");
    }

    protected void clearForms() {
        chestValue.setText("");
        bicepsValue.setText("");
        waistValue.setText("");
        hipsValue.setText("");
        totalWeightValue.setText("");
        muscleWeightValue.setText("");
        fatPercentValue.setText("");
    }

    protected void clearDeltaFields() {
        deltaChestValue.setText("");
        deltaBicepsValue.setText("");
        deltaWaistValue.setText("");
        deltaHipsValue.setText("");
        deltaTotalWeightValue.setText("");
        deltaMuscleWeightValue.setText("");
        deltaFatPercentValue.setText("");
    }

    protected BodyCharacteristics getBodyCharacteristics() {
        BodyCharacteristics bodyCharacteristics = new BodyCharacteristics(DateUtils.trimTime(new Date()).getTimeInMillis());
        bodyCharacteristics.setChestValue(ValueParser.parseFloat(chestValue));
        bodyCharacteristics.setBicepsValue(ValueParser.parseFloat(bicepsValue));
        bodyCharacteristics.setWaistValue(ValueParser.parseFloat(waistValue));
        bodyCharacteristics.setHipsValue(ValueParser.parseFloat(hipsValue));
        bodyCharacteristics.setTotalWeightValue(ValueParser.parseFloat(totalWeightValue));
        bodyCharacteristics.setMuscleWeightValue(ValueParser.parseFloat(muscleWeightValue));
        bodyCharacteristics.setFatPercentValue(ValueParser.parseFloat(fatPercentValue));
        return bodyCharacteristics;
    }

    protected void updateBodyCharacteristicFields(BodyCharacteristics bodyCharacteristics) {
        if (bodyCharacteristics != null) {
            dateOfLastMeasurement.setText(DateFormat.format("dd MMMM yyyy г.", new Date(bodyCharacteristics.getMeasurementDate())));
            chestValue.setText(decimalFormat.format(bodyCharacteristics.getChestValue()));
            bicepsValue.setText(decimalFormat.format(bodyCharacteristics.getBicepsValue()));
            waistValue.setText(decimalFormat.format(bodyCharacteristics.getWaistValue()));
            hipsValue.setText(decimalFormat.format(bodyCharacteristics.getHipsValue()));
            totalWeightValue.setText(decimalFormat.format(bodyCharacteristics.getTotalWeightValue()));
            muscleWeightValue.setText(decimalFormat.format(bodyCharacteristics.getMuscleWeightValue()));
            fatPercentValue.setText(decimalFormat.format(bodyCharacteristics.getFatPercentValue()));
        }else {
            clearForms();
        }
    }

    @Click({R.id.removeStateBtn, R.id.saveStateBtn, R.id.progressPrevArrow, R.id.progressNextArrow})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.removeStateBtn:
                database.removeBodyCharacteristics(currentBodyCharacteristicsValue.getMeasurementDate());
                reloadBodyCharacteristics();
                break;
            case R.id.saveStateBtn:
                BodyCharacteristics bodyCharacteristics = getBodyCharacteristics();
                saveBodyCharacteristic(bodyCharacteristics);
                break;
            case R.id.progressPrevArrow:
                currentBodyCharacteristics.moveToNext();
                reloadBodyCharacteristicValue(currentBodyCharacteristics);
                break;
            case R.id.progressNextArrow:
                currentBodyCharacteristics.moveToPrevious();
                reloadBodyCharacteristicValue(currentBodyCharacteristics);
            default:
                break;
        }
    }

    @Background
    protected void saveBodyCharacteristic(BodyCharacteristics bodyCharacteristics) {
        database.saveBodyCharacteristics(bodyCharacteristics);
        reloadBodyCharacteristics();
    }
}