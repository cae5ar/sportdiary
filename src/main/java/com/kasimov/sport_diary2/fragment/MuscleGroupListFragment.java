package com.kasimov.sport_diary2.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kasimov.sport_diary2.R;
import com.kasimov.sport_diary2.adapter.ExercisesListAdapter;
import com.kasimov.sport_diary2.adapter.MuscleGroupRecyclerAdapter;
import com.kasimov.sport_diary2.model.ExerciseGroup;
import com.kasimov.sport_diary2.util.SimpleCallback;

import java.util.Arrays;


public class MuscleGroupListFragment extends AbstractTabFragment implements CanManageExercises {


    protected String title = "Упражнения";
    private SimpleCallback clickCallback;
    private SimpleCallback removeCallback;
    private ExercisesListAdapter.CheckDelegate checker;
    protected RecyclerView recyclerView;
    private MuscleGroupRecyclerAdapter adapter;

    public MuscleGroupListFragment() {
    }

    public static MuscleGroupListFragment newInstance() {
        return newInstance(null, null, null);
    }

    public static MuscleGroupListFragment newInstance(SimpleCallback clickCallback, SimpleCallback removeCallback, ExercisesListAdapter.CheckDelegate checker) {
        MuscleGroupListFragment instance = new MuscleGroupListFragment();
        instance.setClickCallback(clickCallback);
        instance.setRemoveCallback(removeCallback);
        instance.setChecker(checker);
        return instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflate = inflater.inflate(R.layout.fragment_muscle_group_list, null);
        recyclerView = (RecyclerView) inflate.findViewById(R.id.recyclerView);
        av();
        return inflate;
    }

    public void av() {
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new MuscleGroupRecyclerAdapter(clickCallback, removeCallback, checker, Arrays.asList(ExerciseGroup.values()));
        recyclerView.setAdapter(adapter);
    }

    public void setClickCallback(SimpleCallback clickCallback) {
        this.clickCallback = clickCallback;
    }

    public void setRemoveCallback(SimpleCallback removeCallback) {
        this.removeCallback = removeCallback;
    }

    public void setChecker(ExercisesListAdapter.CheckDelegate checker) {
        this.checker = checker;
    }

    @Override
    public void notifyDataSetChanged() {
        adapter.notifyDataSetChanged();
    }

    @Override
    public void loadShow(boolean asList) {

    }

}