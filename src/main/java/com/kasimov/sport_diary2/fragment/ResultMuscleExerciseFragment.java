package com.kasimov.sport_diary2.fragment;


import android.support.v4.app.Fragment;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.kasimov.sport_diary2.R;
import com.kasimov.sport_diary2.model.Approach;
import com.kasimov.sport_diary2.model.Exercise;
import com.kasimov.sport_diary2.util.SimpleCallback;
import com.kasimov.sport_diary2.util.ValueParser;
import com.kasimov.sport_diary2.view.ApproachView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import info.hoang8f.android.segmented.SegmentedGroup;

/**
 * A simple {@link Fragment} subclass.
 */
@EFragment(R.layout.fragment_result_muscle_exercise)
public class ResultMuscleExerciseFragment extends AbstractEditExerciseFragment {

    @ViewById
    protected TextView repeatsLabel;
    @ViewById
    protected TextView workWeightLabel;
    @ViewById(R.id.approachesLabel)
    protected TextView approachesNumberLabel;
    @ViewById
    protected EditText comment;
    @ViewById
    protected EditText delta;
    @ViewById
    protected LinearLayout appreachesContainer;
    @ViewById
    protected SegmentedGroup deltaGroup;
    @ViewById
    protected TextView weightUnitLabel;
    @ViewById(R.id.minus)
    protected RadioButton minusRadioBtn;

    public static ResultMuscleExerciseFragment getInstance(Exercise exercise) {
        ResultMuscleExerciseFragment resultMuscleExerciseFragment = new ResultMuscleExerciseFragment_();
        resultMuscleExerciseFragment.setExercise(exercise);
        return resultMuscleExerciseFragment;
    }

    public ResultMuscleExerciseFragment() {
    }


    @AfterViews
    protected void updateView() {
        workWeightLabel.setText("Рабочий вес: " + exercise.getWorkingWeight() + " " + exercise.getWeightUnit().title);
        updateApproachesNumberLabel();
        repeatsLabel.setText("Количество повторений:  " + exercise.getRepsMin() + "-" + exercise.getRepsMax());
        comment.setText(exercise.getComment());
        for (int i = 0; i < exercise.getApproachesNumber(); i++) {
            Approach approach = null;
            if (exercise.getApproachesList().size() > i)
                approach = exercise.getApproachesList().get(i);
            addApproachView(i + 1, approach);
        }
        Float delta = exercise.getDelta();
        if (delta == null)
            delta = 0F;
        if (delta < 0) {
            delta *= -1;
            minusRadioBtn.setChecked(true);
        }
        this.delta.setText(delta + "");
        weightUnitLabel.setText(exercise.getWeightUnit().title);
    }

    protected void updateApproachesNumberLabel() {
        approachesNumberLabel.setText("Количество подходов: " + exercise.getApproachesNumber());
    }

    public void addApproach() {
        exercise.setApproachesNumber(exercise.getApproachesNumber() + 1);
        addApproachView(exercise.getApproachesNumber(), null);
        updateApproachesNumberLabel();
    }

    protected void addApproachView(int number, Approach approach) {
        ApproachView approachView = new ApproachView(getActivity(), new SimpleCallback<ApproachView>() {
            @Override
            public void callback(ApproachView value) {
                appreachesContainer.removeView(value);
                exercise.setApproachesNumber(exercise.getApproachesNumber() - 1);
                updateApproachesNumeration();
                updateApproachesNumberLabel();
            }
        });
        approachView.setApproach(approach);
        approachView.setNumber(number);
        appreachesContainer.addView(approachView);
    }


    protected void updateApproachesNumeration() {
        for (int i = 0; i < appreachesContainer.getChildCount(); i++) {
            ApproachView view = (ApproachView) appreachesContainer.getChildAt(i);
            view.setNumber(i + 1);
        }
    }

    protected List<Approach> getApproachesList() {
        List<Approach> list = new ArrayList<>();
        for (int i = 0; i < appreachesContainer.getChildCount(); i++) {
            ApproachView approachView = (ApproachView) appreachesContainer.getChildAt(i);
            list.add(approachView.getApproach());
        }
        return list;
    }

    @Override
    public Exercise getExercise() {
        float parseInt = ValueParser.parseFloat(delta);
        switch (deltaGroup.getCheckedRadioButtonId()) {
            case R.id.plus:
                exercise.setDelta(parseInt);
                break;
            case R.id.minus:
                exercise.setDelta(parseInt * (-1));
                break;
        }
        exercise.setComment(comment.getText().toString());
        exercise.setApproachesList(getApproachesList());
        return exercise;
    }

}