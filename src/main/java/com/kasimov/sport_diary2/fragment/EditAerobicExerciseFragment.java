package com.kasimov.sport_diary2.fragment;


import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.kasimov.sport_diary2.R;
import com.kasimov.sport_diary2.model.Exercise;
import com.kasimov.sport_diary2.model.SpeedUnitType;
import com.kasimov.sport_diary2.util.ValueParser;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

@EFragment(R.layout.fragment_edit_aerobic_exercise)
public class EditAerobicExerciseFragment extends AbstractEditExerciseFragment {

    @ViewById
    protected RadioGroup speedUnitsGroup;
    @ViewById
    protected EditText duration;
    @ViewById
    protected EditText speed;
    @ViewById
    protected RadioButton mileSelector;
    @ViewById
    protected RadioButton kilometerSelector;


    public EditAerobicExerciseFragment() {
    }


    public static EditAerobicExerciseFragment getInstance(Exercise exercise) {
        EditAerobicExerciseFragment fragment = new EditAerobicExerciseFragment_();
        fragment.setExercise(exercise);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void updateView() {
        if (exercise.getDuration() != null) {
            duration.setText(exercise.getDuration() + "");
        }
        if (exercise.getSpeed() != null) {
            switch (exercise.getSpeedUnit()) {
                case KILOMETER_PER_HOUR:
                    kilometerSelector.setChecked(true);
                    break;
                case MILE_PER_HOUR:
                    mileSelector.setChecked(true);
                    break;
            }
            speed.setText(exercise.getSpeed() + "");
        }
    }

    public Exercise getExercise() {
        exercise.setSpeed(ValueParser.parseFloat(speed));
        exercise.setDuration(ValueParser.parseInt(duration));
        switch (speedUnitsGroup.getCheckedRadioButtonId()) {
            case R.id.kilometerSelector:
                exercise.setSpeedUnit(SpeedUnitType.KILOMETER_PER_HOUR);
                break;
            case R.id.mileSelector:
                exercise.setSpeedUnit(SpeedUnitType.MILE_PER_HOUR);
                break;
        }
        return exercise;
    }

    @Click({R.id.btn_add,R.id.btn_minus})
    public void onClick(View view) {
        int parseInt = ValueParser.parseInt(duration);
        switch (view.getId()) {
            case R.id.btn_minus:
                parseInt = parseInt > 0 ? parseInt - 1 : parseInt;
                break;
            case R.id.btn_add:
                parseInt++;
                break;
        }
        duration.setText((parseInt) + "");
    }

}