package com.kasimov.sport_diary2.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Training implements Serializable {
    public static final String TABLE_NAME = "Training";
    public static final String DATE = "date";
    public static final String ID = "_id";
    public static final String TYPE_NAME = "typeName";
    protected long date;
    protected TrainingType type;
    protected Map<IExerciseType, Exercise> exercises = new HashMap<>();
    protected List<Exercise> exercisesToRemove = new ArrayList<>();
    protected Long id;
    protected int isComplete;

    public Training() {
        isComplete = 0;
    }

    public Training(long date) {
        this();
        this.date = date;
    }

    public Training(long date, TrainingType type) {
        this.date = date;
        this.type = type;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public TrainingType getType() {
        return type;
    }

    public void setType(TrainingType type) {
        this.type = type;
    }

    public Map<IExerciseType, Exercise> getExercises() {
        return exercises;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setExercise(List<Exercise> list) {
        exercises.clear();
        for(Exercise exercise: list){
            exercises.put(exercise.getType(), exercise);
        }
    }

    public void removeExercise(Exercise exercise){
        if(exercise.getId() != null)
            exercisesToRemove.add(exercise);
        exercises.remove(exercise.getType());
    }

    public List<Exercise> getExercisesToRemove() {
        return exercisesToRemove;
    }

    public int isComplete() {
        return isComplete;
    }

    public void setComplete(int complete) {
        isComplete = complete;
    }
}