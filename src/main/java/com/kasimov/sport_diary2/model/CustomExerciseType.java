package com.kasimov.sport_diary2.model;

import java.io.Serializable;

// обертка для кастомных упражений
public class CustomExerciseType implements IExerciseType,Serializable {

    protected String key;
    protected String title;
    protected ExerciseGroup group;
    protected String videoName;
    protected boolean amplitudeSize;

    public CustomExerciseType(String title, ExerciseGroup group, String videoName, boolean amplitudeSize) {
        this.title = title;
        this.group = group;
        this.videoName = videoName;
        this.amplitudeSize = amplitudeSize;
    }

    @Override
    public ExerciseGroup getGroup() {
        return group;
    }

    public void setGroup(ExerciseGroup group) {
        this.group = group;
    }

    public String getVideoName() {
        return videoName;
    }

    public void setVideoName(String videoName) {
        this.videoName = videoName;
    }

    public boolean getAmplitudeSize() {
        return amplitudeSize;
    }

    @Override
    public String getKey() {
        return key;
    }

    public void setAmplitudeSize(boolean amplitudeSize) {
        this.amplitudeSize = amplitudeSize;
    }

    @Override
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
