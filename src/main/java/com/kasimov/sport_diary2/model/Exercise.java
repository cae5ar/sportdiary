package com.kasimov.sport_diary2.model;

import android.content.ContentValues;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class Exercise implements Serializable {
    public static final String TABLE_NAME = "Exercise";
    public static final String EXERCISE_TYPE = "exerciseType";
    public static final String WORKING_WEIGHT = "workingWeight";
    public static final String WEIGHT_UNIT_NAME = "weightUnitName";
    public static final String APPROACHES = "approaches";
    public static final String TRAINING_ID = "training_id";
    public static final String REPS_MIN = "repsMin";
    public static final String REPS_MAX = "repsMax";
    public static final String SPEED = "speed";
    public static final String SPEED_UNIT_NAME = "speedUnitName";
    public static final String COMMENT = "comment";
    public static final String AVERAGE_SPEED = "averageSpeed";
    public static final String AVERAGE_DURATION = "averageDuration";
    public static final String DURATION = "duration";
    public static final String DISTANCE = "distance";
    public static final String DISTANCE_UNIT_NAME = "distanceUnitName";
    public static final String DELTA = "delta";
    public static final String HEART_RATE = "heartRate";
    public static final String EXERCISE_ID = "_id";
    protected Long id;
    protected Long trainingId;
    //тип упражения
    protected IExerciseType type;
    //ед. измерения веса
    protected WeightUnitType weightUnit;
    //рабочий вес
    protected Float workingWeight;
    //количество подходов
    protected Integer approaches;
    //количество повторов мин
    protected Integer repsMin;
    //количество повторов макс
    protected Integer repsMax;
    //ед. измерения скорости
    protected SpeedUnitType speedUnit;
    //скорость
    protected Float speed;
    //скорость сред. выдержанной нагрузки
    protected Float averageSpeed;
    //длительность сред. выдержанной нагрузки
    protected Integer averageDuration;
    //длительность пробежки
    protected Integer duration;
    //дистаниця
    protected Float distance;
    //ед. измерения дистанции
    protected DistanceUnitType distanceUnit;
    //разница
    protected Float delta;
    //заметка
    protected String comment;
    //ЧСС
    protected Integer heartRate;
    // подходы
    protected List<Approach> approachesList = new ArrayList<>();
    public static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#.##");

    public IExerciseType getType() {
        return type;
    }

    public void setType(IExerciseType type) {
        this.type = type;
    }

    public Float getWorkingWeight() {
        return workingWeight;
    }

    public void setWorkingWeight(Float workingWeight) {
        this.workingWeight = workingWeight;
    }

    public WeightUnitType getWeightUnit() {
        return weightUnit;
    }

    public void setWeightUnit(WeightUnitType weightUnit) {
        this.weightUnit = weightUnit;
    }

    public SpeedUnitType getSpeedUnit() {
        return speedUnit;
    }

    public void setSpeedUnit(SpeedUnitType speedUnit) {
        this.speedUnit = speedUnit;
    }

    public Float getSpeed() {
        return speed;
    }

    public void setSpeed(Float speed) {
        this.speed = speed;
    }

    public Float getAverageSpeed() {
        return averageSpeed;
    }

    public void setAverageSpeed(Float averageSpeed) {
        this.averageSpeed = averageSpeed;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Float getDistance() {
        return distance;
    }

    public void setDistance(Float distance) {
        this.distance = distance;
    }

    public DistanceUnitType getDistanceUnit() {
        return distanceUnit;
    }

    public void setDistanceUnit(DistanceUnitType distanceUnit) {
        this.distanceUnit = distanceUnit;
    }

    public Float getDelta() {
        return delta;
    }

    public void setDelta(Float delta) {
        this.delta = delta;
    }

    public Integer getApproachesNumber() {
        return approaches;
    }

    public void setApproachesNumber(Integer approaches) {
        this.approaches = approaches;
    }

    public Integer getRepsMin() {
        return repsMin;
    }

    public void setRepsMin(Integer repsMin) {
        this.repsMin = repsMin;
    }

    public Integer getRepsMax() {
        return repsMax;
    }

    public void setRepsMax(Integer repsMax) {
        this.repsMax = repsMax;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getAverageDuration() {
        return averageDuration;
    }

    public void setAverageDuration(Integer averageDuration) {
        this.averageDuration = averageDuration;
    }

    public Integer getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(Integer heartRate) {
        this.heartRate = heartRate;
    }


    public Long getTrainingId() {
        return trainingId;
    }

    public void setTrainingId(Long trainingId) {
        this.trainingId = trainingId;
    }

    public List<Approach> getApproachesList() {
        return approachesList;
    }

    public void setApproachesList(List<Approach> approachesList) {
        this.approachesList = approachesList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ContentValues getContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(Exercise.TRAINING_ID, trainingId);
        contentValues.put(EXERCISE_TYPE, type.getKey());
        if (getWeightUnit() != null) {
            contentValues.put(WORKING_WEIGHT, getWorkingWeight());
            contentValues.put(WEIGHT_UNIT_NAME, getWeightUnit().name());
        }
        contentValues.put(APPROACHES, getApproachesNumber());
        contentValues.put(REPS_MIN, getRepsMin());
        contentValues.put(REPS_MAX, getRepsMax());
        if (getSpeedUnit() != null) {
            contentValues.put(SPEED_UNIT_NAME, getSpeedUnit().name());
            contentValues.put(SPEED, getSpeed());
        }
        contentValues.put(COMMENT, getComment());
        contentValues.put(AVERAGE_SPEED, getAverageSpeed());
        contentValues.put(AVERAGE_DURATION, getAverageDuration());
        contentValues.put(DURATION, getDuration());
        if (getDistanceUnit() != null) {
            contentValues.put(DISTANCE_UNIT_NAME, getDistanceUnit().name());
            contentValues.put(DISTANCE, getDistance());
        }
        contentValues.put(DELTA, getDelta());
        contentValues.put(HEART_RATE, getHeartRate());
        return contentValues;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (getType().getGroup() == ExerciseGroup.AEROBIC) {
            sb.append("Скорость: " + DECIMAL_FORMAT.format(getSpeed()) + getSpeedUnit().title + ", время: " + getDuration() + " мин.");
            if (getComment() != null)
                sb.append("\n" + getComment() );
            if (getAverageDuration() != null || getAverageSpeed() != null) {
                sb.append("\nВыдержанная нагрузка: ");
                if (getAverageSpeed() != null) {
                    sb.append(DECIMAL_FORMAT.format(getAverageSpeed()) + " " + getSpeedUnit().title);
                    if (getAverageDuration() != null)
                        sb.append(", ");
                }
                if (getAverageDuration() != null)
                    sb.append(getAverageDuration() + " мин.");
            }
            if (getHeartRate() != null) {
                sb.append("\nЧСС: " + getHeartRate());
            }
            if (getDistance() != null && getDistanceUnit() != null) {
                sb.append("\nРасстояние: " + DECIMAL_FORMAT.format(getDistance()) + " " + getDistanceUnit().title);
            }
        } else {
            sb.append("Раб. вес: " + DECIMAL_FORMAT.format(getWorkingWeight()) + getWeightUnit().title + ", подходов: " + getApproachesNumber());
            if (getComment() != null && !getComment().isEmpty())
                sb.append("\n" + getComment());
            if(!getApproachesList().isEmpty())
                sb.append("\n");
            for (int i = 0; i < getApproachesList().size(); i++) {
                Approach approach = getApproachesList().get(i);
                sb.append( "\n"+(i + 1) + "-й подход: " + DECIMAL_FORMAT.format(approach.getWeight()) + " " + getWeightUnit().title + ", повторов: " + approach.getRepeats());
            }
        }
        return sb.toString();
    }
}