package com.kasimov.sport_diary2.model;

public enum TrainingType {
    TRAINING_TYPE1("Рост объемов мышц"),
    TRAINING_TYPE2("Рост силовой выносливости"),
    TRAINING_TYPE3("Рост максимальной силы"),
    TRAINING_TYPE4("Шлифовка мышц"),
    TRAINING_TYPE5("Аэробная"),
    TRAINING_TYPE6("Тестовый замер"),
    ;

    public String title;

    TrainingType(String title) {
        this.title = title;
    }
}
