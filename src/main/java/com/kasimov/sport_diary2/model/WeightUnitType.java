package com.kasimov.sport_diary2.model;

public enum WeightUnitType {

    KILOGRAM("кг"),
    POUNDS("lb"),
    PIECES("шт")
    ;

    public String title;

    private WeightUnitType(String title) {
        this.title = title;
    }
}
