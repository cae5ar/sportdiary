package com.kasimov.sport_diary2.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

// сюда закинем стандартные упражения
public enum ExerciseType implements IExerciseType {

    EXERCISE_1(ExerciseGroup.CHEST, "Жим штанги лежа на гориз. скамье", "video_01.mp4", false),
    EXERCISE_2(ExerciseGroup.CHEST, "Жим гантелей лежа на гориз. скамье", "video_02.mp4", false),
    EXERCISE_3(ExerciseGroup.CHEST, "Жим штанги лежа на наклонной скамье", "video_03.mp4", false),
    EXERCISE_4(ExerciseGroup.CHEST, "Жим гантелей лежа на наклонной скамье", "video_04.mp4", false),
    EXERCISE_5(ExerciseGroup.TRICEPS, "Отжимания на брусьях (узкий хват)", "video_05.mp4", false),
    EXERCISE_6(ExerciseGroup.CHEST, "Сведение рук на верхнем блоке стоя (кроссовер)", "video_06.mp4", false),
    EXERCISE_7(ExerciseGroup.BICEPS, "Сгибание рук со штангой стоя узким хватом", "video_07.mp4", false),
    EXERCISE_8(ExerciseGroup.BICEPS, "Сгибание рук с EZ штангой стоя", "video_08.mp4", false),
    EXERCISE_9(ExerciseGroup.BICEPS, "Сгибание рук со штангой узким хватом сидя с упором в ноги", "video_09.mp4", false),
    EXERCISE_10(ExerciseGroup.BICEPS, "Сгибание рук со штангой широким хватом", "video_10.mp4", false),
    EXERCISE_11(ExerciseGroup.BICEPS, "Сгибание рук с ez штангой обратным хватом", "video_11.mp4", false),
    EXERCISE_12(ExerciseGroup.BICEPS, "Сгибание рук со штангой обратным хватом", "video_12.mp4", false),
    EXERCISE_13(ExerciseGroup.BICEPS, "Сгибание рук с гантелями стоя", "video_13.mp4", false),
    EXERCISE_14(ExerciseGroup.FOREARM, "Сгибание рук со штангой обратным хватом", "video_12.mp4", false),
    EXERCISE_15(ExerciseGroup.FOREARM, "Сгибание запястий со штангой.", "video_14.mp4", true),
    EXERCISE_16(ExerciseGroup.TRICEPS, "Французский жим ez штанги лежа", "video_15.mp4", false),
    EXERCISE_17(ExerciseGroup.TRICEPS, "Пулловеры с гантелями", "video_16.mp4", false),
    EXERCISE_18(ExerciseGroup.TRICEPS, "Разгибания рук на верхем блоке (обратный хват)", "video_17.mp4", false),
    EXERCISE_19(ExerciseGroup.BACK, "Подтягивания широким хватом на перекладине", "video_18.mp4", false),
    EXERCISE_20(ExerciseGroup.BACK, "Тяга на верхнем блоке за голову", "video_19.mp4", false),
    EXERCISE_21(ExerciseGroup.BACK, "Подтягивания к груди узким хватом", "video_20.mp4", false),
    EXERCISE_22(ExerciseGroup.BACK, "Тяга к груди на верхнем блоке узким хватом", "video_21.mp4", false),
    EXERCISE_23(ExerciseGroup.BACK, "Тяга штанги в наклоне под углом 45 градусов", "video_22.mp4", false),
    EXERCISE_24(ExerciseGroup.BACK, "Тяга штанги в наклоне", "video_23.mp4", false),
    EXERCISE_25(ExerciseGroup.BACK, "Тяга гантели в наклоне", "video_24.mp4", false),
    EXERCISE_26(ExerciseGroup.TRAPEZIUM, "«Шраги» с гантелями", "video_25.mp4", true),
    EXERCISE_27(ExerciseGroup.SHOULDERS, "Разведение рук с гантелями стоя", "video_26.mp4", false),
    EXERCISE_28(ExerciseGroup.SHOULDERS, "Отведение одной рукой с гантелей стоя", "video_27.mp4", false),
    EXERCISE_29(ExerciseGroup.SHOULDERS, "Жимы с гантелями сидя", "video_28.mp4", false),
    EXERCISE_30(ExerciseGroup.SHOULDERS, "Жим штанги над головой стоя", "video_29.mp4", false),
    EXERCISE_31(ExerciseGroup.SHOULDERS, "Разведение рук с гантелями в наклоне сидя", "video_30.mp4", false),
    EXERCISE_32(ExerciseGroup.SHOULDERS, "Разведение рук на спец. тренажере", "video_31.mp4", false),
    EXERCISE_33(ExerciseGroup.PRESS, "Скручивания лежа на полу", "video_32.mp4", true),
    EXERCISE_34(ExerciseGroup.PRESS, "Подъемы ног в висе на перекладине", "video_33.mp4", false),
    EXERCISE_35(ExerciseGroup.PRESS, "Подъемы ног в висе на шведской стенке", "video_34.mp4", false),
    EXERCISE_36(ExerciseGroup.PRESS, "Подъемы коленей в висе на перекладине", "video_35.mp4", false),
    EXERCISE_37(ExerciseGroup.PRESS, "Подъемы ног в висе с поворотом", "video_36.mp4", false),
    EXERCISE_38(ExerciseGroup.PRESS, "Сгибания туловища вбок на тренажере «Гиперэкстензия»", "video_37.mp4", false),
    EXERCISE_39(ExerciseGroup.PRESS, "Наклоны с гантелей в стороны стоя", "video_38.mp4", false),
    EXERCISE_40(ExerciseGroup.PRESS, "Повороты с палочкой", "video_39.mp4", false),
    EXERCISE_41(ExerciseGroup.HIPS, "Приседания со штангой в узкой стойке", "video_40.mp4", false),
    EXERCISE_42(ExerciseGroup.HIPS, "Приседания со штангой в широкой стойке", "video_41.mp4", false),
    EXERCISE_43(ExerciseGroup.HIPS, "Жимы ногами в тренажере в узкой стойке", "video_42.mp4", false),
    EXERCISE_44(ExerciseGroup.HIPS, "Жимы ногами в тренажере в широкой стойке", "video_43.mp4", false),
    EXERCISE_45(ExerciseGroup.HIPS, "Выпрямление ног в тренажере", "video_44.mp4", false),
    EXERCISE_46(ExerciseGroup.HIPS, "Сгибание ног в тренажере лежа", "video_45.mp4", false),
    EXERCISE_47(ExerciseGroup.SPAWN, "Подъемы на носки сидя в спец. тренажере", "video_46.mp4", true),
    EXERCISE_48(ExerciseGroup.HIPS, "Сгибание одной ногой в тренажере стоя", "video_47.mp4", true),
    EXERCISE_49(ExerciseGroup.TRICEPS, "Разгибания рук с ez грифом из-за головы стоя", "video_48.mp4", false),
    EXERCISE_50(ExerciseGroup.TRICEPS, "Трицепсовый жим лежа", "video_49.mp4", false),
    EXERCISE_51(ExerciseGroup.TRICEPS, "Разгибания рук на верхнем блоке (хват под углом)", "video_50.mp4", false),
    EXERCISE_52(ExerciseGroup.TRICEPS, "Разгибания рук на верхнем блоке (с веревкой)", "video_51.mp4", false),
    EXERCISE_53(ExerciseGroup.TRICEPS, "Разгибания рук на верхнем блоке (прямой хват)", "video_52.mp4", false),
    EXERCISE_54(ExerciseGroup.CHEST, "Жим в грудном тренажере вверх", "video_53.mp4", false),
    EXERCISE_55(ExerciseGroup.BICEPS, "Сгибание одной рукой вбок (молоток)", "video_54.mp4", false),
    EXERCISE_56(ExerciseGroup.BICEPS, "Сгибание рук с гантелями (молоток)", "video_55.mp4", false),
    EXERCISE_57(ExerciseGroup.HIPS, "Разгибания на тренажере гиперэкстензия", "video_56.mp4", false),
    EXERCISE_58(ExerciseGroup.BACK, "Разгибания на тренажере гиперэкстензия", "video_56.mp4", false),
    EXERCISE_59(ExerciseGroup.SHOULDERS, "Махи гантелей в наклоне в сторону одной рукой", "video_57.mp4", false),
    EXERCISE_60(ExerciseGroup.SHOULDERS, "Протяжка для боковых пучков плеча", "video_58.mp4", false),
    EXERCISE_61(ExerciseGroup.SHOULDERS, "Жим штанги в тренажере сидя", "video_59.mp4", false),
    EXERCISE_62(ExerciseGroup.BICEPS, "Сгибание рук на тренажере LARRY-SCOTT", "video_60.mp4", false),
    EXERCISE_63(ExerciseGroup.AEROBIC, "Бег", "video_61.mp4", false),
    EXERCISE_64(ExerciseGroup.AEROBIC, "Ходьба", null, false),
    EXERCISE_65(ExerciseGroup.AEROBIC, "Беговая дорожка", null, false),
    EXERCISE_66(ExerciseGroup.AEROBIC, "Ходьба/Бег на эллипсоиде", null, false),
    EXERCISE_67(ExerciseGroup.AEROBIC, "Ходьба на степпере", null, false),
    EXERCISE_68(ExerciseGroup.AEROBIC, "Плавание", null, false),
    EXERCISE_69(ExerciseGroup.AEROBIC, "Скандинавская ходьба", null, false),
    EXERCISE_70(ExerciseGroup.CHEST, "Жимы с гантелями на скамье с обратным наклоном (угол 30 градусов)", "video_62.mp4", false),
    EXERCISE_71(ExerciseGroup.CHEST, "Жимы с гантелями на скамье с обратным наклоном (угол 15 градусов)", "video_63.mp4", false),
    EXERCISE_72(ExerciseGroup.CHEST, "Жим штанги лежа (негативы)", "video_64.mp4", false),
    EXERCISE_73(ExerciseGroup.CHEST, "Разводки с гантелями на наклонной скамье (угол 45 градусов)", "video_65.mp4", false),
    EXERCISE_74(ExerciseGroup.CHEST, "Разводки с гантелями на наклонной скамье (угол 30 градусов)", "video_66.mp4", false),
    EXERCISE_75(ExerciseGroup.CHEST, "Разводки с гантелями на горизонтальной скамье", "video_67.mp4", false),
    EXERCISE_76(ExerciseGroup.CHEST, "Отжимания на брусьях (широкий хват)", "video_68.mp4", false),
    EXERCISE_77(ExerciseGroup.CHEST, "Отжимания от пола", "video_69.mp4", false),
    EXERCISE_78(ExerciseGroup.HIPS, "Выпады с гантелями", "video_70.mp4", false),
    EXERCISE_79(ExerciseGroup.HIPS, "Глубокие приседания на платформах с гирей/гантелей", "video_71.mp4", false),
    EXERCISE_80(ExerciseGroup.HIPS, "Махи ногой назад стоя на корточках", "video_72.mp4", false),
    EXERCISE_81(ExerciseGroup.HIPS, "Мертвая тяга", "video_73.mp4", false),
    EXERCISE_82(ExerciseGroup.BICEPS, "Подтягивания на перекладине", "video_74.mp4", false),
    EXERCISE_83(ExerciseGroup.BICEPS, "Сгибание рук с ez штангой стоя у стены", "video_75.mp4", false),
    EXERCISE_84(ExerciseGroup.BICEPS, "Сгибание рук со штангой стоя у стены", "video_76.mp4", false),
    EXERCISE_85(ExerciseGroup.BICEPS, "Сгибания рук со штангой стоя (негативы)", "video_77.mp4", false),
    EXERCISE_86(ExerciseGroup.SPAWN, "Подъемы на носки стоя на платформе", "video_78.mp4", true),
    EXERCISE_87(ExerciseGroup.SHOULDERS, "Махи гантелей назад (наклон вперед)", "video_79.mp4", false),
    EXERCISE_88(ExerciseGroup.SHOULDERS, "Махи с блином вперед", "video_80.mp4", false),
    EXERCISE_89(ExerciseGroup.SHOULDERS, "Махи с гантелями вперед (1 рука)", "video_81.mp4", false),
    EXERCISE_90(ExerciseGroup.SHOULDERS, "Махи с гантелями вперед (2 руки)", "video_82.mp4", false),
    EXERCISE_91(ExerciseGroup.BACK, "Лодочки", "video_83.mp4", false),
    EXERCISE_92(ExerciseGroup.BACK, "Тяга верхнего блока к животу (наклон вниз под углом 45 градусов)", "video_84.mp4", false),
    EXERCISE_93(ExerciseGroup.BACK, "Тяга нижнего блока к животу", "video_85.mp4", false),
    EXERCISE_94(ExerciseGroup.TRICEPS, "Обратные отжимания от скамьи", "video_86.mp4", false),
    EXERCISE_95(ExerciseGroup.TRICEPS, "Французский жим ez штанги лежа (негативы)", "video_87.mp4", false),;

    public String title;
    public ExerciseGroup group;
    public String videoName;
    public boolean type;


    ExerciseType(ExerciseGroup group, String title, String videoName, boolean type) {
        this.group = group;
        this.title = title;
        this.videoName = videoName;
        this.type = type;
    }

    public static List<ExerciseType> getExercisesByGroup(ExerciseGroup group){
        List<ExerciseType> out = new ArrayList<>();
        for (ExerciseType exerciseType : values()){
            if(exerciseType.getGroup()==group)
                out.add(exerciseType);
        }
        Collections.sort(out, new Comparator<ExerciseType>() {
            @Override
            public int compare(ExerciseType o1, ExerciseType o2) {
                return o1.getTitle().compareTo(o2.getTitle());
            }
        });
        return out;
    }
    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public ExerciseGroup getGroup() {
        return group;
    }

    @Override
    public String getVideoName() {
        return videoName;
    }

    public boolean getAmplitudeSize() {
        return type;
    }

    @Override
    public String getKey(){
        return name();
    }

    public static List<ExerciseType> valuesSorted() {
        List<ExerciseType> result = Arrays.asList(values());
        Collections.sort(result, new Comparator<ExerciseType>() {
            @Override
            public int compare(ExerciseType o1, ExerciseType o2) {
                return o1.getTitle().compareTo(o2.getTitle());
            }
        });
        return result;
    }
}