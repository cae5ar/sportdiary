package com.kasimov.sport_diary2.model;

public interface IExerciseType{
    // название
    String getTitle();
    // группа
    ExerciseGroup getGroup();
    // название видео
    String getVideoName();
    //тип амплитуды
    boolean getAmplitudeSize();
    // уникальный идентификатор
    String getKey();
}
