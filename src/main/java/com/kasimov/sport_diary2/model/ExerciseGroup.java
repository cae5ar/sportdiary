package com.kasimov.sport_diary2.model;

public enum ExerciseGroup {

    AEROBIC("Аэробные"),
    BICEPS("Бицепсы"),
    BACK("Спина"),
    SPAWN("Икры"),
    TRAPEZIUM("Трапеции"),
    HIPS("Бедра"),
    FOREARM("Предплечья"),
    SHOULDERS("Плечи"),
    PRESS("Пресс"),
    TRICEPS("Трицепсы"),
    CHEST("Грудь"),
    ;
    public String title;

    private ExerciseGroup(String title) {
        this.title = title;
    }
}