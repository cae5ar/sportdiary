package com.kasimov.sport_diary2.model;

public enum SpeedUnitType {
    MILE_PER_HOUR("м/ч"),
    KILOMETER_PER_HOUR("км/ч");

    public String title;

    private SpeedUnitType(String title) {
        this.title = title;
    }
}
