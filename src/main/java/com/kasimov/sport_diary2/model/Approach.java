package com.kasimov.sport_diary2.model;

import java.io.Serializable;

public class Approach implements Serializable {
    public static final String TABLE_NAME = "Approach";
    public static final String EXERCISE_ID = "exercise_id";
    public static final String WEIGHT = "weight";
    public static final String REPEATS = "repeats";
    //weight
    protected float weight;
    //количество повторов мин
    protected int repeats;

    public Approach() {
    }

    public Approach(float weight, int repeats) {
        this.weight = weight;
        this.repeats = repeats;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public int getRepeats() {
        return repeats;
    }

    public void setRepeats(int repeats) {
        this.repeats = repeats;
    }

    @Override
    public Approach clone(){
        return new Approach(weight,repeats);
    }
}
