package com.kasimov.sport_diary2.model;

import java.io.Serializable;


public class BodyCharacteristics implements Serializable {
    public static final String TABLE_NAME = "BodyCharacteristics";
    public static final String MEASUREMENT_DATE = "measurementDate";
    public static final String CHEST_VALUE = "chestValue";
    public static final String BICEPS_VALUE = "bicepsValue";
    public static final String WAIST_VALUE = "waistValue";
    public static final String HIPS_VALUE = "hipsValue";
    public static final String TOTAL_WEIGHT_VALUE = "totalWeightValue";
    public static final String MUSCLE_WEIGHT_VALUE = "muscleWeightValue";
    public static final String FAT_PERCENT_VALUE = "fatPercentValue";
    //дата замера
    private long measurementDate;
    //грудь
    private float chestValue;
    //бицепс
    private float bicepsValue;
    //талия
    private float waistValue;
    //бедра
    private float hipsValue;

    // вес тела
    private float totalWeightValue;
    // вес мышц
    private float muscleWeightValue;
    // процент жира
    private float fatPercentValue;

    public BodyCharacteristics() {
        chestValue=0.0F;
        bicepsValue=0.0F;
        waistValue=0.0F;
        hipsValue=0.0F;
        totalWeightValue=0.0F;
        muscleWeightValue=0.0F;
        fatPercentValue=0.0F;
    }

    public BodyCharacteristics(long measurementDate) {
        this.measurementDate = measurementDate;
    }

    public BodyCharacteristics(float chestValue, float bicepsValue, float waistValue, float hipsValue, float totalWeightValue, float muscleWeightValue, float fatPercentValue) {
        this.chestValue = chestValue;
        this.bicepsValue = bicepsValue;
        this.waistValue = waistValue;
        this.hipsValue = hipsValue;
        this.totalWeightValue = totalWeightValue;
        this.muscleWeightValue = muscleWeightValue;
        this.fatPercentValue = fatPercentValue;
    }

    public float getChestValue() {
        return chestValue;
    }

    public void setChestValue(float chestValue) {
        this.chestValue = chestValue;
    }

    public float getBicepsValue() {
        return bicepsValue;
    }

    public void setBicepsValue(float bicepsValue) {
        this.bicepsValue = bicepsValue;
    }

    public float getWaistValue() {
        return waistValue;
    }

    public void setWaistValue(float waistValue) {
        this.waistValue = waistValue;
    }

    public float getHipsValue() {
        return hipsValue;
    }

    public void setHipsValue(float hipsValue) {
        this.hipsValue = hipsValue;
    }

    public float getTotalWeightValue() {
        return totalWeightValue;
    }

    public void setTotalWeightValue(float totalWeightValue) {
        this.totalWeightValue = totalWeightValue;
    }

    public float getMuscleWeightValue() {
        return muscleWeightValue;
    }

    public void setMuscleWeightValue(float muscleWeightValue) {
        this.muscleWeightValue = muscleWeightValue;
    }

    public float getFatPercentValue() {
        return fatPercentValue;
    }

    public void setFatPercentValue(float fatPercentValue) {
        this.fatPercentValue = fatPercentValue;
    }

    public long getMeasurementDate() {
        return measurementDate;
    }

    public void setMeasurementDate(long measurementDate) {
        this.measurementDate = measurementDate;
    }
}
