package com.kasimov.sport_diary2.model;

public enum DistanceUnitType {
    KILOMETER("км"),
    MILE("мили");

    public String title;

    DistanceUnitType(String title) {
        this.title = title;
    }
}
