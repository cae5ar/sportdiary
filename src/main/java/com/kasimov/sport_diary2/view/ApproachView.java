package com.kasimov.sport_diary2.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kasimov.sport_diary2.R;
import com.kasimov.sport_diary2.model.Approach;
import com.kasimov.sport_diary2.util.SimpleCallback;
import com.kasimov.sport_diary2.util.ValueParser;

public class ApproachView extends LinearLayout implements View.OnClickListener{

    protected final Context ctx;
    protected final LayoutInflater li;
    private SimpleCallback callback;
    protected final TextView approachNumber;
    private final EditText weight;
    private final EditText repeats;

    public ApproachView(Context context, SimpleCallback<ApproachView> callback) {
        super(context);
        ctx = context;
        li = LayoutInflater.from(context);
        this.callback = callback;
        li.inflate(R.layout.approach_edit, this);
        LinearLayout relativeLayout = (LinearLayout)getChildAt(0);
        approachNumber = (TextView)relativeLayout.findViewById(R.id.approachNumber);
        weight = (EditText)relativeLayout.findViewById(R.id.weight);
        repeats = (EditText)relativeLayout.findViewById(R.id.repeats);
        relativeLayout.findViewById(R.id.deleteApproach).setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.deleteApproach)
            callback.callback(this);
    }

    public Approach getApproach(){
        return new Approach(ValueParser.parseFloat(weight),ValueParser.parseInt(repeats));
    }

    public void setApproach(Approach approach){
        if(approach!=null) {
            weight.setText(approach.getWeight() + "");
            repeats.setText(approach.getRepeats() + "");
        }
    }

    public void setNumber(int i) {
        approachNumber.setText(i+"-й подход");
    }
}