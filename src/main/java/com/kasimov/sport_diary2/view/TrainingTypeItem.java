package com.kasimov.sport_diary2.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kasimov.sport_diary2.R;
import com.kasimov.sport_diary2.model.TrainingType;
import com.kasimov.sport_diary2.util.SimpleCallback;

public class TrainingTypeItem extends RelativeLayout {

    private final TextView text;
    private final CheckBox checkbox;
    private final Context ctx;
    LayoutInflater li;
    private TrainingType trainingType;
    private RelativeLayout itemRoot;

    public TrainingTypeItem(Context context) {
        super(context);
        ctx = context;
        li = LayoutInflater.from(context);
        li.inflate(R.layout.trainig_type_view, this);
        itemRoot = (RelativeLayout)getChildAt(0);
        text = (TextView) itemRoot.getChildAt(0);
        checkbox = (CheckBox)  itemRoot.getChildAt(1);
    }

    public TrainingType getTrainingType() {
        return trainingType;
    }

    public TrainingTypeItem(Context context, TrainingType trainingType) {
        this(context);
        this.trainingType = trainingType;
        text.setText(trainingType.title);
    }

    public void setClickCallback(final SimpleCallback callback){
        OnClickListener l = new OnClickListener() {
            @Override
            public void onClick(View view) {
                clicked();
                callback.callback(TrainingTypeItem.this);
            }
        };
        checkbox.setOnClickListener(l);
        setOnClickListener(l);
    }
    
    public void setLastBackground(){
        itemRoot.setBackgroundResource(R.drawable.item_panel_top_bottom_border);
    };

    private void clicked() {
        setSelected(true);
    }

    public void setSelected(boolean selected){
        checkbox.setChecked(selected);
    }

    public boolean isSelected(){
        return checkbox.isSelected();
    }
}
