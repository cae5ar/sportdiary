package com.kasimov.sport_diary2.view;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kasimov.sport_diary2.R;
import com.kasimov.sport_diary2.activity.VideoPlayerActivity;
import com.kasimov.sport_diary2.adapter.ExercisesListAdapter;
import com.kasimov.sport_diary2.model.ExerciseGroup;
import com.kasimov.sport_diary2.model.ExerciseType;
import com.kasimov.sport_diary2.util.SimpleCallback;

import static com.kasimov.sport_diary2.util.AppConstants.VIDEO_NAME;


public class ExerciseTypeItem extends FrameLayout{


    protected SimpleCallback clickCallback;
    protected SimpleCallback removeCallback;
    protected ExercisesListAdapter.CheckDelegate checkDelegate;
    protected final Context ctx;
    protected final LayoutInflater li;
    private final RelativeLayout lastExerciseItemView;

    public ExerciseTypeItem(Context context, final ExerciseType exerciseType,final SimpleCallback clickCallback, final SimpleCallback removeCallback,final ExercisesListAdapter.CheckDelegate checkDelegate) {
        super(context);
        ctx = context;
        li = LayoutInflater.from(context);

        lastExerciseItemView = (RelativeLayout) li.inflate(R.layout.exercise_item, null);
        ImageView imageView = (ImageView) lastExerciseItemView.findViewById(R.id.exerciseIcon);
        if (exerciseType.getGroup() == ExerciseGroup.AEROBIC)
            imageView.setImageResource(R.drawable.aero);
        TextView textView = (TextView) lastExerciseItemView.findViewById(R.id.exerciseName);
        textView.setText(exerciseType.getTitle());
        if (clickCallback != null) {
            lastExerciseItemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    clickCallback.callback(exerciseType);
                }
            });
        }
        ImageView exerciseRemoveIcon = (ImageView) lastExerciseItemView.findViewById(R.id.exerciseRemoveIcon);
        if (removeCallback != null){
            exerciseRemoveIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    removeCallback.callback(exerciseType);
                }
            });
        }
        if(checkDelegate != null){
            exerciseRemoveIcon.setVisibility(checkDelegate.check(exerciseType) ? View.VISIBLE : View.GONE);
        }
        if (exerciseType.getVideoName() != null) {
            ImageView exerciseVideoIcon = (ImageView) lastExerciseItemView.findViewById(R.id.exerciseVideoIcon);
            exerciseVideoIcon.setVisibility(View.VISIBLE);
            exerciseVideoIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ctx, VideoPlayerActivity.class);
                    intent.putExtra(VIDEO_NAME, exerciseType.getVideoName());
                    ctx.startActivity(intent);
                }
            });
        }
        setItemBackground(R.drawable.item_panel_top_border);
        addView(lastExerciseItemView);
    }

    public void setItemBackground(int backgroundResource) {
        lastExerciseItemView.setBackgroundResource(backgroundResource);
    }
}
