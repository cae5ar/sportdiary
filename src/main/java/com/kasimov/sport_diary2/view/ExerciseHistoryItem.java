package com.kasimov.sport_diary2.view;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kasimov.sport_diary2.R;
import com.kasimov.sport_diary2.model.Exercise;
import com.kasimov.sport_diary2.model.TrainingType;

import java.text.DecimalFormat;

public class ExerciseHistoryItem extends FrameLayout {
    private final Context ctx;
    private final LayoutInflater li;
    private final DecimalFormat decimalFormat;
    private RelativeLayout rootView;

    public ExerciseHistoryItem(Context context, Exercise exercise, TrainingType trainingType) {
        super(context);
        ctx = context;
        li = LayoutInflater.from(context);
        decimalFormat = new DecimalFormat("#.##");
        setExercise(exercise, trainingType);
    }

    protected void setExercise(Exercise exercise, TrainingType training) {
        rootView = (RelativeLayout) li.inflate(R.layout.exercise_history_item, null);
        TextView delta = (TextView) rootView.findViewById(R.id.delta);
        if (exercise.getDelta() > 0) {
            delta.setText("+" + decimalFormat.format(exercise.getDelta()));
            delta.setTextColor(ContextCompat.getColor(ctx, R.color.colorDeltaPositive));
        } else {
            delta.setText(decimalFormat.format(exercise.getDelta()));
            delta.setTextColor(ContextCompat.getColor(ctx, exercise.getDelta() == 0 ? R.color.colorDeltaZero : R.color.colorDeltaNegative));
        }
        TextView exerciseName = (TextView) rootView.findViewById(R.id.exerciseName);
        exerciseName.setText(exercise.getType().getTitle());
        TextView trainingType = (TextView) rootView.findViewById(R.id.trainingType);
        trainingType.setText(training.title);
        LinearLayout textContainer = (LinearLayout) rootView.findViewById(R.id.textContainer);
        TextView textView = new TextView(ctx);
        textView.setText(exercise.toString());
        textContainer.addView(textView);
        addView(rootView);
    }

    public View getRootView() {
        return rootView;
    }
}