package com.kasimov.sport_diary2.util;

public interface SimpleCallback<T> {
    void callback(T value);
}
