package com.kasimov.sport_diary2.util;

public class AppConstants {

    public static final int EDIT_TRAINING_CODE = 1;
    public static final int EDIT_EXERCISE_CODE = 2;

    public static final String ATLAS = "По группам";
    public static final String LIST = "По алфавиту";
    public static final String VIDEO_NAME = "videoName";

    public static final String TRAINING_KEY = "com.kasimov.sport_diary.model.Training";
    public static final String EXERCISE_KEY = "com.kasimov.sport_diary.model.Exercise";
}
