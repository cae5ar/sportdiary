package com.kasimov.sport_diary2.util;

import android.widget.EditText;


public class ValueParser {

    public static int parseInt(EditText text) {
        int parseInt = 0;
        try{
            parseInt = Integer.parseInt(text.getText().toString());
        }catch (Exception e){}
        return parseInt;
    }


    public static float parseFloat(EditText text) {
        float parseInt = 0;
        try{
            parseInt = Float.parseFloat(text.getText().toString());
        }catch (Exception e){}
        return parseInt;
    }
}
