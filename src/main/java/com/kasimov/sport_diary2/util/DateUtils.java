package com.kasimov.sport_diary2.util;


import android.support.annotation.NonNull;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateUtils {

    public static Calendar trimTime(long dateTime) {
        Calendar instance = GregorianCalendar.getInstance();
        instance.setTimeInMillis(dateTime);
        return trimTime(instance);
    }
    public static Calendar trimTime(Date date) {
        Calendar instance = GregorianCalendar.getInstance();
        instance.set(date.getYear()+1900, date.getMonth(), date.getDate());
        return trimTime(instance);
    }

    @NonNull
    public static Calendar trimTime(Calendar calendar) {
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar;
    }
}
