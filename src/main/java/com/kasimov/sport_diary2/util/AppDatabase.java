package com.kasimov.sport_diary2.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.support.annotation.NonNull;
import android.util.Log;

import com.kasimov.sport_diary2.model.Approach;
import com.kasimov.sport_diary2.model.BodyCharacteristics;
import com.kasimov.sport_diary2.model.DistanceUnitType;
import com.kasimov.sport_diary2.model.Exercise;
import com.kasimov.sport_diary2.model.ExerciseType;
import com.kasimov.sport_diary2.model.SpeedUnitType;
import com.kasimov.sport_diary2.model.Training;
import com.kasimov.sport_diary2.model.TrainingType;
import com.kasimov.sport_diary2.model.WeightUnitType;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public class AppDatabase extends SQLiteAssetHelper {

    private static AppDatabase instance;

    public static AppDatabase getInstance(Context context) {
        if (instance == null)
            instance = new AppDatabase(context);
        return instance;
    }

    private static final String TAG = "AppDatabase";

    private static final String DATABASE_NAME = "sportdiary.db";
    private static final int DATABASE_VERSION = 2;
    public static String[] APPROACH_COLUMNS = {Approach.WEIGHT, Approach.REPEATS};

    public static String[] EXERCISE_COLUMNS = {
            Exercise.EXERCISE_ID,
            Exercise.TRAINING_ID,
            Exercise.EXERCISE_TYPE,
            Exercise.WORKING_WEIGHT,
            Exercise.WEIGHT_UNIT_NAME,
            Exercise.APPROACHES,
            Exercise.REPS_MIN,
            Exercise.REPS_MAX,
            Exercise.SPEED,
            Exercise.SPEED_UNIT_NAME,
            Exercise.COMMENT,
            Exercise.AVERAGE_SPEED,
            Exercise.AVERAGE_DURATION,
            Exercise.DURATION,
            Exercise.DISTANCE,
            Exercise.DISTANCE_UNIT_NAME,
            Exercise.DELTA,
            Exercise.HEART_RATE
    };
    public static final String[] TRAINING_COLUMNS = new String[]{
            Training.ID,
            Training.DATE,
            Training.TYPE_NAME
    };
    public static final String[] BODY_CHARACTERISTICS_COLUMNS = new String[]{
            BodyCharacteristics.MEASUREMENT_DATE,
            BodyCharacteristics.CHEST_VALUE,
            BodyCharacteristics.BICEPS_VALUE,
            BodyCharacteristics.WAIST_VALUE,
            BodyCharacteristics.HIPS_VALUE,
            BodyCharacteristics.TOTAL_WEIGHT_VALUE,
            BodyCharacteristics.MUSCLE_WEIGHT_VALUE,
            BodyCharacteristics.FAT_PERCENT_VALUE
    };

    private AppDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    public Training getTrainingByDate(long date) {
        SQLiteDatabase db = getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        String selection = "date =" + date;
        String sqlTables = "Training";
        qb.setTables(sqlTables);
        Cursor c = qb.query(db, TRAINING_COLUMNS, selection, null, null, null, null);
        Training training = new Training(date);
        if (c.moveToFirst()) {
            extractTraining(c, training);
        }
        c.close();
        return training;
    }

    public Training extractTraining(Cursor c, Training training) {
        training.setId(c.getLong(0));
        training.setDate(c.getLong(1));
        training.setType(TrainingType.valueOf(c.getString(2)));
        List<Exercise> exercises = getExercises(training.getId());
        training.setExercise(exercises);
        return training;
    }

    @NonNull
    public List<Exercise> getExercises(Long trainingId) {
        List<Exercise> exercises = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.appendWhere(Exercise.TRAINING_ID + "=" + trainingId);
        qb.setTables(Exercise.TABLE_NAME);
        Cursor mainCursor = qb.query(db, EXERCISE_COLUMNS, null, null, null, null, null);
        if (mainCursor.moveToFirst()) {
            do {
                int column = 0;
                Exercise exercise = new Exercise();
                exercise.setId(mainCursor.getLong(column++));
                exercise.setTrainingId(mainCursor.getLong(column++));
                String s = mainCursor.getString(column++);
                exercise.setType(s == null ? null : ExerciseType.valueOf(s));
                exercise.setWorkingWeight(mainCursor.getFloat(column++));
                s = mainCursor.getString(column++);
                exercise.setWeightUnit(s == null ? null : WeightUnitType.valueOf(s));
                exercise.setApproachesNumber(mainCursor.getInt(column++));
                exercise.setRepsMin(mainCursor.getInt(column++));
                exercise.setRepsMax(mainCursor.getInt(column++));
                exercise.setSpeed(mainCursor.getFloat(column++));
                s = mainCursor.getString(column++);
                exercise.setSpeedUnit(s == null ? null : SpeedUnitType.valueOf(s));
                exercise.setComment(mainCursor.getString(column++));
                exercise.setAverageSpeed(mainCursor.getFloat(column++));
                exercise.setAverageDuration(mainCursor.getInt(column++));
                exercise.setDuration(mainCursor.getInt(column++));
                exercise.setDistance(mainCursor.getFloat(column++));
                s = mainCursor.getString(column++);
                exercise.setDistanceUnit(s == null ? null : DistanceUnitType.valueOf(s));
                exercise.setDelta(mainCursor.getFloat(column++));
                exercise.setHeartRate(mainCursor.getInt(column++));

                SQLiteQueryBuilder subquery = new SQLiteQueryBuilder();
                subquery.setTables(Approach.TABLE_NAME);
                subquery.appendWhere(Approach.EXERCISE_ID + "=" + exercise.getId());
                Cursor subCursor = subquery.query(db, APPROACH_COLUMNS, null, null, null, null, null);
                if (subCursor.moveToFirst()) {
                    do {
                        exercise.getApproachesList().add(new Approach(subCursor.getFloat(0), subCursor.getInt(1)));
                    } while (subCursor.moveToNext());
                }
                subCursor.close();
                exercises.add(exercise);
            } while (mainCursor.moveToNext());
        }
        mainCursor.close();
        return exercises;
    }

    public Training saveTraining(Training training) {
        ContentValues cv = new ContentValues();
        cv.put(Training.DATE, training.getDate());
        cv.put(Training.TYPE_NAME, training.getType().name());
        SQLiteDatabase writableDatabase = getWritableDatabase();
        if (training.getId() == null) {
            training.setId(writableDatabase.insert(Training.TABLE_NAME, null, cv));
            Log.d(TAG, Training.TABLE_NAME + ": inserted row with rowid = " + training.getId());
        } else {
            writableDatabase.update(Training.TABLE_NAME, cv, Training.ID + " = " + training.getId(), null);
            Log.d(TAG, Training.TABLE_NAME + ": updated row with rowid = " + training.getId());
        }
        saveExercises(writableDatabase, training.getExercises().values(), training.getId());
        removeExercises(writableDatabase, training.getExercisesToRemove());
        return training;
    }

    public void removeExercises(SQLiteDatabase writableDatabase, List<Exercise> exercisesToRemove) {
        String ids = "(";
        for (int i = 0; i < exercisesToRemove.size(); i++) {
            if (i > 0)
                ids += ", ";
            ids += exercisesToRemove.get(i).getId();

        }
        ids += ")";
        int deletedRows = getWritableDatabase().delete(Exercise.TABLE_NAME, Exercise.EXERCISE_ID + " in " + ids, null);
        Log.d(TAG, Exercise.TABLE_NAME + ": deleted rows =  " + deletedRows);
    }

    public void saveExercises(SQLiteDatabase writableDatabase, Collection<Exercise> exercises, long trainingId) {
        for (Exercise exercise : exercises) {
            saveExercise(writableDatabase, trainingId, exercise);
        }
    }

    @NonNull
    public Exercise saveExercise(Exercise exercise) {
        return saveExercise(getWritableDatabase(), exercise.getTrainingId(), exercise);
    }

    protected Exercise saveExercise(SQLiteDatabase writableDatabase, long trainingId, Exercise exercise) {
        exercise.setTrainingId(trainingId);
        ContentValues values = exercise.getContentValues();
        if (exercise.getId() == null) {
            exercise.setId(writableDatabase.insert(Exercise.TABLE_NAME, null, values));
            Log.d(TAG, Exercise.TABLE_NAME + ": inserted row with rowid = " + exercise.getId());
        } else {
            writableDatabase.update(Exercise.TABLE_NAME, values, Exercise.EXERCISE_ID + " = " + exercise.getId(), null);
            Log.d(TAG, Exercise.TABLE_NAME + ": updated row with rowid = " + exercise.getId());
        }
        saveApproaches(writableDatabase, exercise.getApproachesList(), exercise.getId());
        return exercise;
    }

    public void saveApproaches(SQLiteDatabase writableDatabase, List<Approach> approaches, long exercise_id) {
        int deletedRows = writableDatabase.delete(Approach.TABLE_NAME, Approach.EXERCISE_ID + " = " + exercise_id, null);
        Log.d(TAG, Approach.TABLE_NAME + ": deleted rows =  " + deletedRows);
        for (Approach approach : approaches) {
            ContentValues cv = new ContentValues();
            cv.put(Approach.EXERCISE_ID, exercise_id);
            cv.put(Approach.WEIGHT, approach.getWeight());
            cv.put(Approach.REPEATS, approach.getRepeats());
            long insert = writableDatabase.insert(Approach.TABLE_NAME, null, cv);
            Log.d(TAG, Approach.TABLE_NAME + ": inserted row with rowid = " + insert);
        }
    }

    public Cursor getTrainingHistory() {
        SQLiteDatabase db = getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.appendWhere("exists(select 1 from " + Exercise.TABLE_NAME + " as ex where ex." + Exercise.TRAINING_ID + " = tr." + Training.ID + " and ex." + Exercise.DELTA + " is not null)");
        qb.setTables(Training.TABLE_NAME + " as tr ");
        Cursor mainCursor = qb.query(db, TRAINING_COLUMNS, null, null, null, null, Training.DATE + " DESC");
        return mainCursor;
    }

    public List<Date> getTrainingDates() {
        SQLiteDatabase db = getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(Training.TABLE_NAME + " as tr ");
        Cursor mainCursor = qb.query(db, new String[]{Training.DATE}, null, null, null, null, null);
        List<Date> dates = new ArrayList<>();
        if (mainCursor.moveToFirst()) {
            do {
                dates.add(new Date(mainCursor.getLong(0)));
            } while (mainCursor.moveToNext());
        }
        mainCursor.close();
        return dates;
    }

    public void removeBodyCharacteristics(long timeInMillis) {
        int deletedRows = getWritableDatabase().delete(BodyCharacteristics.TABLE_NAME, BodyCharacteristics.MEASUREMENT_DATE + " = " + timeInMillis, null);
        Log.d(TAG, BodyCharacteristics.TABLE_NAME + ": deleted rows =  " + deletedRows);
    }

    public void saveBodyCharacteristics(BodyCharacteristics bodyCharacteristics) {
        removeBodyCharacteristics(bodyCharacteristics.getMeasurementDate());
        ContentValues cv = new ContentValues();
        cv.put(BodyCharacteristics.MEASUREMENT_DATE, bodyCharacteristics.getMeasurementDate());
        cv.put(BodyCharacteristics.CHEST_VALUE, bodyCharacteristics.getChestValue());
        cv.put(BodyCharacteristics.BICEPS_VALUE, bodyCharacteristics.getBicepsValue());
        cv.put(BodyCharacteristics.WAIST_VALUE, bodyCharacteristics.getWaistValue());
        cv.put(BodyCharacteristics.HIPS_VALUE, bodyCharacteristics.getHipsValue());
        cv.put(BodyCharacteristics.TOTAL_WEIGHT_VALUE, bodyCharacteristics.getTotalWeightValue());
        cv.put(BodyCharacteristics.MUSCLE_WEIGHT_VALUE, bodyCharacteristics.getMuscleWeightValue());
        cv.put(BodyCharacteristics.FAT_PERCENT_VALUE, bodyCharacteristics.getFatPercentValue());
        long insert = getWritableDatabase().insert(BodyCharacteristics.TABLE_NAME, null, cv);
        Log.d(TAG, BodyCharacteristics.TABLE_NAME + ": inserted row with rowid = " + insert);
    }

    public BodyCharacteristics getFirstBodyCharacteristics(Long date) {
        BodyCharacteristics bodyCharacteristics = null;
        SQLiteDatabase db = getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        if (date != null)
            qb.appendWhere(BodyCharacteristics.MEASUREMENT_DATE + ">=" + date);
        qb.setTables(BodyCharacteristics.TABLE_NAME + " as bc ");
        Cursor mainCursor = qb.query(db, BODY_CHARACTERISTICS_COLUMNS, null, null, null, null, BodyCharacteristics.MEASUREMENT_DATE + " ASC");
        if (mainCursor.moveToFirst()) {
            bodyCharacteristics = extractBodyCharacteristics(mainCursor);
        }
        mainCursor.close();
        return bodyCharacteristics;
    }


    public Cursor getLatestBodyCharacteristics() {
        SQLiteDatabase db = getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(BodyCharacteristics.TABLE_NAME + " as bc ");
        Cursor mainCursor = qb.query(db, BODY_CHARACTERISTICS_COLUMNS, null, null, null, null, BodyCharacteristics.MEASUREMENT_DATE + " DESC");
        mainCursor.moveToFirst();
        return mainCursor;
    }

    @NonNull
    public BodyCharacteristics extractBodyCharacteristics(Cursor mainCursor) {
        if (mainCursor.getCount() < 1)
            return null;
        BodyCharacteristics bodyCharacteristics;
        int column = 0;
        bodyCharacteristics = new BodyCharacteristics();
        bodyCharacteristics.setMeasurementDate(mainCursor.getLong(column++));
        bodyCharacteristics.setChestValue(mainCursor.getFloat(column++));
        bodyCharacteristics.setBicepsValue(mainCursor.getFloat(column++));
        bodyCharacteristics.setWaistValue(mainCursor.getFloat(column++));
        bodyCharacteristics.setHipsValue(mainCursor.getFloat(column++));

        bodyCharacteristics.setTotalWeightValue(mainCursor.getFloat(column++));
        bodyCharacteristics.setMuscleWeightValue(mainCursor.getFloat(column++));
        bodyCharacteristics.setFatPercentValue(mainCursor.getFloat(column++));
        return bodyCharacteristics;
    }

    public Training removeTraining(Training trainingForSelectedDate) {
        int deletedRows = getWritableDatabase().delete(Training.TABLE_NAME, Training.ID + " = " + trainingForSelectedDate.getId(), null);
        Log.d(TAG, Training.TABLE_NAME + ": deleted rows =  " + deletedRows);
        return new Training(trainingForSelectedDate.getDate());
    }
}