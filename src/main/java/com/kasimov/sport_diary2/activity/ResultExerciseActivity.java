package com.kasimov.sport_diary2.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.kasimov.sport_diary2.R;
import com.kasimov.sport_diary2.fragment.AbstractEditExerciseFragment;
import com.kasimov.sport_diary2.fragment.ResultAeroExerciseFragment;
import com.kasimov.sport_diary2.fragment.ResultMuscleExerciseFragment;
import com.kasimov.sport_diary2.model.Exercise;
import com.kasimov.sport_diary2.model.ExerciseGroup;
import com.kasimov.sport_diary2.util.AppConstants;

public class ResultExerciseActivity extends AppCompatActivity implements View.OnClickListener {

    private Exercise exercise;
    private AbstractEditExerciseFragment editExerciseFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_exercise);
        exercise = (Exercise) getIntent().getSerializableExtra(AppConstants.EXERCISE_KEY);
        initUI();

    }

    private void initUI() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        TextView textView = (TextView) findViewById(R.id.exerciseName);
        textView.setText(exercise.getType().getTitle());
        selectFragment();
        findViewById(R.id.saveExerciseBtn).setOnClickListener(this);
    }

    private void selectFragment() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (exercise.getType().getGroup() == ExerciseGroup.AEROBIC) {
            editExerciseFragment = ResultAeroExerciseFragment.getInstance(exercise);
            fragmentTransaction.add(R.id.exercise_result_fragment_container, editExerciseFragment);
        } else {
            editExerciseFragment = ResultMuscleExerciseFragment.getInstance(exercise);
            fragmentTransaction.add(R.id.exercise_result_fragment_container, editExerciseFragment);
        }
        fragmentTransaction.commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                setResult(RESULT_CANCELED);
                finish();
                return true;
            case R.id.action_add_approach:
                ((ResultMuscleExerciseFragment)editExerciseFragment).addApproach();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if(exercise.getType().getGroup() != ExerciseGroup.AEROBIC)
            getMenuInflater().inflate(R.menu.menu_add_approach_btn, menu);
        return true;
    }

    @Override
    public void onClick(View view) {
        Exercise exercise = editExerciseFragment.getExercise();
        Intent intent = new Intent();
        intent.putExtra(AppConstants.EXERCISE_KEY, exercise);
        setResult(RESULT_OK, intent);
        finish();
    }
}
