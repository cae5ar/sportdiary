package com.kasimov.sport_diary2.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.kasimov.sport_diary2.R;
import com.kasimov.sport_diary2.fragment.AbstractEditExerciseFragment;
import com.kasimov.sport_diary2.fragment.EditAerobicExerciseFragment;
import com.kasimov.sport_diary2.fragment.EditMuscleExerciseFragment;
import com.kasimov.sport_diary2.model.Exercise;
import com.kasimov.sport_diary2.model.ExerciseGroup;
import com.kasimov.sport_diary2.util.AppConstants;

public class EditExerciseActivity extends AppCompatActivity implements View.OnClickListener{

    AbstractEditExerciseFragment editExerciseFragment;
    protected Exercise exercise;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_exercise);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Intent intent = getIntent();
        exercise = (Exercise) intent.getSerializableExtra(AppConstants.EXERCISE_KEY);
        selectFragment();
        findViewById(R.id.saveExerciseBtn).setOnClickListener(this);
        setTitle(exercise.getType().getTitle());
    }

    private void selectFragment() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if(exercise.getType().getGroup()== ExerciseGroup.AEROBIC) {
            editExerciseFragment = EditAerobicExerciseFragment.getInstance(exercise);
            fragmentTransaction.add(R.id.exercise_edit_fragment_container, editExerciseFragment);
        }
        else {
            editExerciseFragment = EditMuscleExerciseFragment.getInstance(exercise);
            fragmentTransaction.add(R.id.exercise_edit_fragment_container, editExerciseFragment);
        }
        fragmentTransaction.commit();
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent();
        Exercise exercise = editExerciseFragment.getExercise();
        intent.putExtra(AppConstants.EXERCISE_KEY, exercise);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                setResult(RESULT_CANCELED);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}