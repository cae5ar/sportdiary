package com.kasimov.sport_diary2.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import com.kasimov.sport_diary2.R;
import com.kasimov.sport_diary2.adapter.CustomSpinnerAdapter;
import com.kasimov.sport_diary2.adapter.ExercisesListAdapter;
import com.kasimov.sport_diary2.fragment.MainActivityExercisesFragment;
import com.kasimov.sport_diary2.model.Exercise;
import com.kasimov.sport_diary2.model.ExerciseType;
import com.kasimov.sport_diary2.model.IExerciseType;
import com.kasimov.sport_diary2.model.Training;
import com.kasimov.sport_diary2.util.AppConstants;
import com.kasimov.sport_diary2.util.SimpleCallback;

import java.util.Arrays;

public class ExercisesActivity extends AppCompatActivity {

    protected Training selectedTraining;
    protected Menu menuTopMenuActionBar;
    protected SimpleCallback<IExerciseType> clickCallback = new SimpleCallback<IExerciseType>() {
        @Override
        public void callback(IExerciseType value) {
            Intent intent = new Intent(ExercisesActivity.this, EditExerciseActivity.class);
            Exercise exercise = null;
            if (!selectedTraining.getExercises().containsKey(value)) {
                exercise = new Exercise();
                exercise.setType(value);
            } else
                exercise = selectedTraining.getExercises().get(value);
            intent.putExtra(AppConstants.EXERCISE_KEY, exercise);
            ExercisesActivity.this.startActivityForResult(intent, AppConstants.EDIT_TRAINING_CODE);

        }
    };

    protected SimpleCallback<IExerciseType> removeCallback = new SimpleCallback<IExerciseType>() {
        @Override
        public void callback(IExerciseType value) {
            if (selectedTraining.getExercises().containsKey(value)) {
                Exercise exercise = selectedTraining.getExercises().get(value);
                selectedTraining.removeExercise(exercise);
            }
            menuTopMenuActionBar.findItem(R.id.action_save).setVisible(!selectedTraining.getExercises().isEmpty());
        }
    };
    protected ExercisesListAdapter.CheckDelegate checkDelegate = new ExercisesListAdapter.CheckDelegate() {
        @Override
        public boolean check(ExerciseType exerciseType) {
            return selectedTraining.getExercises().containsKey(exerciseType);
        }
    };
    private MainActivityExercisesFragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercises);
        selectedTraining = (Training) getIntent().getSerializableExtra(AppConstants.TRAINING_KEY);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        setTitle(selectedTraining.getType().title);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.disallowAddToBackStack();
        fragment = MainActivityExercisesFragment.newInstance(clickCallback, removeCallback, checkDelegate);
        fragmentTransaction.replace(R.id.fragment_container, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                Intent intent = new Intent();
                intent.putExtra(AppConstants.TRAINING_KEY, selectedTraining);
                setResult(RESULT_OK, intent);
                finish();
                return true;
            case android.R.id.home:
                setResult(RESULT_CANCELED);
                finish();
                return true;
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menuTopMenuActionBar = menu;
        Spinner spinner_nav = (Spinner) findViewById(R.id.spinner_nav);
        CustomSpinnerAdapter adapter = new CustomSpinnerAdapter(getApplicationContext(), Arrays.asList(AppConstants.LIST, AppConstants.ATLAS));
        spinner_nav.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapter, View v,
                                       int position, long id) {
                String item = adapter.getItemAtPosition(position).toString();
                fragment.loadShow(item.equals(AppConstants.LIST));
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                Toast.makeText(getApplicationContext(), "Selected  : NOTHING",
                        Toast.LENGTH_LONG).show();
            }
        });
        spinner_nav.setAdapter(adapter);
        getMenuInflater().inflate(R.menu.menu_exercise_list, menuTopMenuActionBar);
        menuTopMenuActionBar.findItem(R.id.action_save).setVisible(!selectedTraining.getExercises().isEmpty());
        return true;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (resultCode) {
            case RESULT_OK: {
                menuTopMenuActionBar.findItem(R.id.action_save).setVisible(true);
                Exercise editExercise = (Exercise) data.getSerializableExtra(AppConstants.EXERCISE_KEY);
                if (selectedTraining.getExercises().containsKey(editExercise.getType()))
                    selectedTraining.getExercises().remove(editExercise.getType());
                selectedTraining.getExercises().put(editExercise.getType(), editExercise);
                fragment.notifyDataSetChanged();
            }
        }
    }
}