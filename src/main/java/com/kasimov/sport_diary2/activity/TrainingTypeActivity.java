package com.kasimov.sport_diary2.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kasimov.sport_diary2.R;
import com.kasimov.sport_diary2.model.Training;
import com.kasimov.sport_diary2.model.TrainingType;
import com.kasimov.sport_diary2.util.AppConstants;
import com.kasimov.sport_diary2.util.SimpleCallback;
import com.kasimov.sport_diary2.view.TrainingTypeItem;

import java.util.ArrayList;
import java.util.List;


public class TrainingTypeActivity extends AppCompatActivity {

    protected List<TrainingTypeItem> list = new ArrayList<>();
    protected Training training;
    protected TrainingTypeItem selectedTrainingType;
    protected Button continueAction;
    protected SimpleCallback<TrainingTypeItem> stringSimpleCallback = new SimpleCallback<TrainingTypeItem>() {
        @Override
        public void callback(TrainingTypeItem value) {
            selectTrainingType(value);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        training = (Training) getIntent().getSerializableExtra(AppConstants.TRAINING_KEY);
        setContentView(R.layout.activity_traing_type);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        LinearLayout relativeLayout = (LinearLayout) findViewById(R.id.training_container);
        for (TrainingType trainingType : TrainingType.values()) {
            addTrainingGroup(relativeLayout, stringSimpleCallback, trainingType);
        }
        list.get(list.size()-1).setLastBackground();
        continueAction = (Button) findViewById(R.id.continue_btn);
        continueAction.setEnabled(false);
        continueAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(TrainingTypeActivity.this, ExercisesActivity.class);
                training.setType(selectedTrainingType.getTrainingType());
                intent.putExtra(AppConstants.TRAINING_KEY, training);
                startActivityForResult(intent, AppConstants.EDIT_TRAINING_CODE);
            }
        });
        if(training.getType() != null){
            for (TrainingTypeItem trainingTypeItem : list) {
                if (trainingTypeItem.getTrainingType() == training.getType()) {
                    selectTrainingType(trainingTypeItem);
                }
            }
        }
    }

    protected void selectTrainingType(TrainingTypeItem value) {
        for (TrainingTypeItem trainingGroupView : list) {
            if (!trainingGroupView.equals(value)) {
                trainingGroupView.setSelected(false);
            }
        }
        value.setSelected(true);
        TrainingTypeActivity.this.selectedTrainingType = value;
        TextView foo = (TextView)findViewById(R.id.trainingAdviceText);
        foo.setText(Html.fromHtml(getText(selectedTrainingType.getTrainingType())));
        continueAction.setEnabled(true);
    }

    private String getText(TrainingType trainingType) {
        int textId = 0;
        switch (trainingType) {
            case TRAINING_TYPE1:
                textId =  R.string.training_description1;
                break;
            case TRAINING_TYPE2:
                textId = R.string.training_description2;
                break;
            case TRAINING_TYPE3:
                textId = R.string.training_description3;
                break;
            case TRAINING_TYPE4:
                textId = R.string.training_description4;
                break;
            case TRAINING_TYPE5:
                textId = R.string.training_description5;
                break;
            case TRAINING_TYPE6:
                textId = R.string.training_description6;
                break;
            default:
                textId = 0;
        }
        return getString(textId);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                setResult(RESULT_CANCELED);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void addTrainingGroup(LinearLayout relativeLayout, SimpleCallback stringSimpleCallback, TrainingType trainingType) {
        TrainingTypeItem trainingTypeItem = new TrainingTypeItem(this, trainingType);
        trainingTypeItem.setClickCallback(stringSimpleCallback);
        relativeLayout.addView(trainingTypeItem);
        Toast.makeText(this, trainingType.title, Toast.LENGTH_SHORT);
        list.add(trainingTypeItem);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (resultCode){
            case RESULT_OK:
                Training training = (Training) data.getSerializableExtra(AppConstants.TRAINING_KEY);
                training.setType(selectedTrainingType.getTrainingType());
                Intent intent = new Intent();
                intent.putExtra(AppConstants.TRAINING_KEY,training);
                setResult(RESULT_OK, intent);
                finish();
                return;
            case RESULT_CANCELED:
                return;
        }
    }
}
