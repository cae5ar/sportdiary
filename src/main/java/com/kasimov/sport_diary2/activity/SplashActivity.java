package com.kasimov.sport_diary2.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.ads.MobileAds;
import com.kasimov.sport_diary2.R;
import com.kasimov.sport_diary2.util.AppDatabase;

import org.androidannotations.annotations.EActivity;

@EActivity
public class SplashActivity extends AppCompatActivity {

    protected final long SPLASH_DISPLAY_LENGTH = 1000L;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MobileAds.initialize(getApplicationContext(), getString(R.string.addMob_app_id));
        AppDatabase.getInstance(this);
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                Intent intent = MainActivity_.intent(SplashActivity.this).get();
                SplashActivity.this.startActivity(intent);
                SplashActivity.this.finish();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }
}