package com.kasimov.sport_diary2.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import com.kasimov.sport_diary2.R;
import com.kasimov.sport_diary2.provider.CustomAPEZProvider;
import com.kasimov.sport_diary2.util.AppConstants;

public class VideoPlayerActivity extends AppCompatActivity implements MediaPlayer.OnErrorListener, OnPreparedListener {

    private static final int MY_PERMISSIONS_REQUEST_READ_SDCARD = 101;
    private VideoView myVideoView;
    private int position = 0;
    private ProgressDialog progressDialog;
    private MediaController mediaControls;
    private String videoName;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        videoName = getIntent().getStringExtra(AppConstants.VIDEO_NAME);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        setContentView(R.layout.activity_video_player);

        if (mediaControls == null) {
            mediaControls = new MediaController(VideoPlayerActivity.this);
        }
        myVideoView = (VideoView) findViewById(R.id.video_view);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            startVideo();
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_READ_SDCARD);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_SDCARD: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startVideo();
                } else {
                    Toast.makeText(this, "Вы не дали доступ к хранилищу, воспроизведение видео невозможно", Toast.LENGTH_LONG);
                    setResult(RESULT_OK, null);
                    finish();
                }
                return;
            }
        }
    }

    private void startVideo() {
        progressDialog = new ProgressDialog(VideoPlayerActivity.this);
        progressDialog.setTitle("Загрузка видео");
        progressDialog.setMessage("Пожалуйста подождите...");
        progressDialog.show();
        myVideoView.setMediaController(mediaControls);
        myVideoView.setVideoURI(CustomAPEZProvider.buildUri(videoName));
        myVideoView.requestFocus();
        myVideoView.setOnPreparedListener(this);
        myVideoView.setOnErrorListener(this);
    }


    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putInt("Position", myVideoView.getCurrentPosition());
        myVideoView.pause();
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        position = savedInstanceState.getInt("Position");
        myVideoView.seekTo(position);
    }

    public void onPrepared(MediaPlayer mp) {
        progressDialog.dismiss();
        myVideoView.seekTo(position);
        if (position == 0) {
            myVideoView.start();
        } else {
            myVideoView.pause();
        }
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        progressDialog.dismiss();
        finish();
        return true;
    }
}
