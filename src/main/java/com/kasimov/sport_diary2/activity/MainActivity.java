package com.kasimov.sport_diary2.activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionMenu;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.kasimov.sport_diary2.R;
import com.kasimov.sport_diary2.adapter.CustomSpinnerAdapter;
import com.kasimov.sport_diary2.adapter.SectionsPagerAdapter;
import com.kasimov.sport_diary2.fragment.MainActivityCalendarFragment;
import com.kasimov.sport_diary2.fragment.MainActivityExercisesFragment;
import com.kasimov.sport_diary2.model.Exercise;
import com.kasimov.sport_diary2.model.Training;
import com.kasimov.sport_diary2.util.AppConstants;
import com.kasimov.sport_diary2.util.AppDatabase;
import com.kasimov.sport_diary2.util.IabBroadcastReceiver;
import com.kasimov.sport_diary2.util.IabHelper;
import com.kasimov.sport_diary2.util.IabResult;
import com.kasimov.sport_diary2.util.Inventory;
import com.kasimov.sport_diary2.util.Purchase;
import com.kasimov.sport_diary2.view.CustomViewPager;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static com.kasimov.sport_diary2.util.AppConstants.EDIT_EXERCISE_CODE;
import static com.kasimov.sport_diary2.util.AppConstants.EDIT_TRAINING_CODE;
import static com.kasimov.sport_diary2.util.AppConstants.EXERCISE_KEY;
import static com.kasimov.sport_diary2.util.AppConstants.TRAINING_KEY;
import static com.kasimov.sport_diary2.util.IabHelper.IABHELPER_USER_CANCELLED;

@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, IabBroadcastReceiver.IabBroadcastListener, View.OnClickListener {

    public static final int DELAY_BEFORE_EXIT = 2000;
    private static final String TAG = "SportDiary";
    static final String SKU_NOADS = "noads";
    static final int PURCHASE_REQ_CODE = 10001;
    public static final int AD_SHOW_INTERVAL = 30 * 1000;
    public static final String APP_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsVXjoqm6Lf7VjggKdAnk3eLJ2JS7vJY2PaVFuPN2Rxur6hTtQK6daspSrMAAyL0+9/zJS9JVoYIG9EmaAISKZSPw+W3fuQdWcnIJPu2lwVVg2poMSHsKEMaQ5k/vf+qXGqMFBSTfl4BXCpIn9knpsgyVA7Nq/4BmEtJvNCqKZWGcaJljy7BWXlM+4PhAwd0NaxfEpQjcCK0fQZr2PT26C1N5lK10xf2tXAxNzE2RsD8HsqjpDGMaeLHFCQoKXWzymIQTWlTdhn+buGGGQoeXeSfRtVyqILuMHFKHW1nSzVjyyC/aYNbeusStah4A5I33g3iS91a/XtY6GWG3sRU7SwIDAQAB";
    protected long adShowLastTime = 0;

    InterstitialAd mInterstitialAd;
    @ViewById(R.id.container)
    protected CustomViewPager viewPager;
    @ViewById(R.id.loadingPanel)
    protected RelativeLayout loadingPanel;
    @ViewById(R.id.adView)
    protected AdView mAdView;
    @ViewById(R.id.mainActivityRootPanel)
    protected RelativeLayout mainActivityRootPanel;
    @ViewById
    protected Button getProgrammBtn;
    @ViewById
    protected Button removeAds;
    @ViewById(R.id.drawer_layout)
    DrawerLayout drawer;

    protected Training trainingForSelectedDate;
    protected AppDatabase appDatabase;
    protected MainActivityCalendarFragment activityCalendarFragment;
    protected MainActivityExercisesFragment activityExercisesFragment;
    protected Long lastBackPressed;

    protected boolean noAds = false;
    protected boolean canBuyNoAds = true;
    protected IabHelper mHelper;
    protected IabBroadcastReceiver mBroadcastReceiver;
    private Spinner spinnerNav;
    private Purchase premiumPurchase;
    private CustomSpinnerAdapter spinnerAdapter;

    @UiThread
    protected void setWaitScreen(boolean set) {
        loadingPanel.setVisibility(set ? View.VISIBLE : View.GONE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkNoads();
        updateUi();
        setupAds();
        setupIABHelper();
        appDatabase = AppDatabase.getInstance(this);
    }

    private void setupAds() {
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.interstitial_ad_unit_id));
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                requestNewInterstitial();
            }
        });
        requestNewInterstitial();
    }

    protected void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder().build();
        mInterstitialAd.loadAd(adRequest);
    }

    private void setupIABHelper() {
        Log.d(TAG, "Creating IAB helper.");
        mHelper = new IabHelper(this, APP_PUBLIC_KEY);
        mHelper.enableDebugLogging(true, TAG);
        Log.d(TAG, "Starting setup.");
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                Log.d(TAG, "Setup finished.");
                if (!result.isSuccess()) {
                    Log.e(TAG, "Problem setting up in-app billing: " + result);
                    canBuyNoAds = false;
                    return;
                }
                if (mHelper == null) return;
                mBroadcastReceiver = new IabBroadcastReceiver(MainActivity.this);
                IntentFilter broadcastFilter = new IntentFilter(IabBroadcastReceiver.ACTION);
                registerReceiver(mBroadcastReceiver, broadcastFilter);
                Log.d(TAG, "Setup successful. Querying inventory.");
                try {
                    mHelper.queryInventoryAsync(mGotInventoryListener);
                } catch (IabHelper.IabAsyncInProgressException e) {
                    complain("Ошибка восставновления покупки");
                }
            }
        });
    }

    @UiThread
    protected void updateUi() {
        setVisibilityRemoveAdsButton(canBuyNoAds && !noAds);
        if (noAds)
            mAdView.setVisibility(View.GONE);
    }

    protected void setVisibilityRemoveAdsButton(boolean visibilityMode) {
        removeAds.setVisibility(visibilityMode ? View.VISIBLE : View.GONE);
    }

    @AfterViews
    protected void afterViews() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        initNavigationView(toolbar);
        setTitle("Календарь");
        getProgrammBtn.setPaintFlags(getProgrammBtn.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        mAdView.loadAd(new AdRequest.Builder().build());
        viewPager.setAdapter(new SectionsPagerAdapter(getSupportFragmentManager()));
        viewPager.setPagingEnabled(false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            reportFullyDrawn();
        }
    }

    @UiThread
    protected void databaseLoadComplete(AppDatabase instance) {
        appDatabase = instance;
        viewPager.setAdapter(new SectionsPagerAdapter(getSupportFragmentManager()));
        viewPager.setPagingEnabled(false);
    }

    protected void initNavigationView(Toolbar toolbar) {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_calendar);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            long backPressed = new Date().getTime();
            if (lastBackPressed != null) {
                if (backPressed - lastBackPressed < DELAY_BEFORE_EXIT)
                    super.onBackPressed();
                else
                    lastBackPressed = null;
            }
            if (lastBackPressed == null) {
                Toast.makeText(this, "Нажмите назад снова чтобы закрыть приложение", Toast.LENGTH_SHORT).show();
                lastBackPressed = backPressed;
            }
        }
    }

    @Override
    public void receivedBroadcast() {
        // Received a broadcast notification that the inventory of items has changed
        Log.d(TAG, "Received broadcast notification. Querying inventory.");
        try {
            mHelper.queryInventoryAsync(mGotInventoryListener);
        } catch (IabHelper.IabAsyncInProgressException e) {
            Log.e(TAG, "Error querying inventory. Another async operation in progress.", e);
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int selectedFragment = item.getItemId();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        spinnerNav.setVisibility(selectedFragment == R.id.nav_exercises ? View.VISIBLE : View.GONE);
        mAdView.setVisibility(selectedFragment == R.id.nav_calendar || noAds ? View.GONE : View.VISIBLE);
        if (selectedFragment == R.id.nav_calendar) {
            viewPager.setCurrentItem(0);
            setTitle("Календарь");
        } else if (selectedFragment == R.id.nav_exercises) {
            String spinnerItem = spinnerAdapter.getItem(spinnerNav.getSelectedItemPosition());
            activityExercisesFragment.setViewMode(spinnerItem.equals(AppConstants.LIST));
            viewPager.setCurrentItem(1);
            setTitle("Упражнения");
        } else if (selectedFragment == R.id.nav_history) {
            setTitle("История");
            viewPager.setCurrentItem(2);
        } else if (selectedFragment == R.id.nav_progress) {
            setTitle("Прогресс");
            viewPager.setCurrentItem(3);
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        spinnerNav = (Spinner) findViewById(R.id.spinnerNav);
        spinnerAdapter = new CustomSpinnerAdapter(getApplicationContext(), Arrays.asList(AppConstants.LIST, AppConstants.ATLAS));
        spinnerNav.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapter, View v,
                                       int position, long id) {
                String item = adapter.getItemAtPosition(position).toString();
                activityExercisesFragment.loadShow(item.equals(AppConstants.LIST));
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                Toast.makeText(getApplicationContext(), "Selected  : NOTHING",
                        Toast.LENGTH_LONG).show();
            }
        });
        spinnerNav.setAdapter(spinnerAdapter);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.addTrainingBtn: {
                Intent intent = new Intent(this, TrainingTypeActivity.class);
                intent.putExtra(AppConstants.TRAINING_KEY, trainingForSelectedDate);
                this.startActivityForResult(intent, EDIT_TRAINING_CODE);
            }
            break;
            case R.id.editTraining: {
                FloatingActionMenu menuTrainingActions = (FloatingActionMenu) findViewById(R.id.menuTrainingActions);
                menuTrainingActions.close(true);
                Intent intent = new Intent(this, TrainingTypeActivity.class);
                intent.putExtra(AppConstants.TRAINING_KEY, trainingForSelectedDate);
                this.startActivityForResult(intent, EDIT_TRAINING_CODE);
            }
            break;
            case R.id.removeTraining: {
                FloatingActionMenu menuTrainingActions = (FloatingActionMenu) findViewById(R.id.menuTrainingActions);
                menuTrainingActions.close(true);
                removeTraining();
            }
            break;
            default:
        }

    }

    @Background
    protected void removeTraining() {
        trainingForSelectedDate = appDatabase.removeTraining(trainingForSelectedDate);
        onTrainingsListChangeComplete(appDatabase.getTrainingDates());
        onTrainingLoadingComplete();
    }

    @Background
    protected void saveExercises(Exercise exerciseToSave) {
        appDatabase.saveExercise(exerciseToSave);
        onTrainingLoadingComplete();
    }

    @Background
    protected void saveTraining(Training trainingToSave) {
        trainingForSelectedDate = appDatabase.saveTraining(trainingToSave);
        onTrainingLoadingComplete();
    }

    public void loadTrainingForSelectedDate(long timeInMillis) {
        trainingForSelectedDate = appDatabase.getTrainingByDate(timeInMillis);
        onTrainingLoadingComplete();
    }

    @Background
    public void loadTrainingDates() {
        onTrainingsListChangeComplete(appDatabase.getTrainingDates());
    }

    @UiThread
    protected void onTrainingsListChangeComplete(List<Date> dateList) {
        activityCalendarFragment.highlightTrainingDates(dateList);
    }

    @UiThread
    public void onTrainingLoadingComplete() {
        activityCalendarFragment.updateExercisesBlock(trainingForSelectedDate);
    }

    public void setActivityCalendarFragment(MainActivityCalendarFragment activityCalendarFragment) {
        this.activityCalendarFragment = activityCalendarFragment;
    }

    public void setActivityExercisesFragment(MainActivityExercisesFragment activityExercisesFragment) {
        this.activityExercisesFragment = activityExercisesFragment;
    }

    public void editExerciseResult(Exercise item) {
        Intent intent = new Intent(this, ResultExerciseActivity.class);
        intent.putExtra(EXERCISE_KEY, item);
        startActivityForResult(intent, EDIT_EXERCISE_CODE);
    }

    @Click(R.id.removeAds)
    public void onUpgradeAppButtonClicked() {
        drawer.closeDrawer(GravityCompat.START);
        Log.d(TAG, "Upgrade button clicked; launching purchase flow for upgrade.");
        setWaitScreen(true);
        try {
            mHelper.launchPurchaseFlow(this, SKU_NOADS, PURCHASE_REQ_CODE, mPurchaseFinishedListener, "");
        } catch (IabHelper.IabAsyncInProgressException e) {
            complain("Ошибка при соединении с Google Play.");
            setWaitScreen(false);
        }
    }

    @Click(R.id.getProgrammBtn)
    public void onGetProgramButtonClicked() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.programmUri)));
        startActivity(browserIntent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult(" + requestCode + "," + resultCode + "," + data);
        switch (requestCode) {
            case EDIT_TRAINING_CODE:
                switch (resultCode) {
                    case RESULT_OK:
                        saveTraining((Training) data.getSerializableExtra(TRAINING_KEY));
                        break;
                }
                break;
            case EDIT_EXERCISE_CODE:
                switch (resultCode) {
                    case RESULT_OK:
                        saveExercises((Exercise) data.getSerializableExtra(AppConstants.EXERCISE_KEY));
                        break;
                }
                break;
            case PURCHASE_REQ_CODE: {
                if (mHelper == null) break;
                if (mHelper.handleActivityResult(requestCode, resultCode, data)) {
                    Log.d(TAG, "onActivityResult handled by IABUtil.");
                }
                break;
            }

        }
        if (requestCode != PURCHASE_REQ_CODE)
            showIntersitialAd();
    }

    @UiThread
    protected void showIntersitialAd() {
        long now = new Date().getTime();
        if (!noAds && ((now - adShowLastTime) > AD_SHOW_INTERVAL) && mInterstitialAd.isLoaded()) {
            adShowLastTime = now;
            mInterstitialAd.show();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        appDatabase.close();
        try {
            if (mBroadcastReceiver != null) {
                unregisterReceiver(mBroadcastReceiver);
            }
            Log.d(TAG, "Destroying helper.");
            if (mHelper != null) {
                mHelper.disposeWhenFinished();
                mHelper = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void saveData() {
        SharedPreferences.Editor spe = getPreferences(MODE_PRIVATE).edit();
        spe.putBoolean(SKU_NOADS, noAds);
        spe.apply();
    }

    void checkNoads() {
        SharedPreferences sp = getPreferences(MODE_PRIVATE);
        noAds = sp.getBoolean(SKU_NOADS, false);
    }

    void complain(String message) {
        Log.e(TAG, "**** SportDiary Error: " + message);
        alert("Ошибка: " + message);
    }

    @UiThread
    void alert(String message) {
        AlertDialog.Builder bld = new AlertDialog.Builder(this);
        bld.setMessage(message);
        bld.setNeutralButton("OK", null);
        Log.d(TAG, "Showing alert dialog: " + message);
        bld.create().show();
    }

    protected IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            Log.d(TAG, "Purchase finished: " + result + ", purchase: " + purchase);
            // if we were disposed of in the meantime, quit.
            if (mHelper == null) return;
            if (result.isFailure()) {
                setWaitScreen(false);
                if (result.getResponse() == IABHELPER_USER_CANCELLED)
                    return;
                complain("Ошибка покупки: " + result);
                return;
            }
            premiumPurchase = purchase;
            Log.d(TAG, "Purchase successful.");
            Log.d(TAG, "Purchase is premium upgrade. Congratulating user.");
            alert("Спасибо за поддержку");
            noAds = true;
            saveData();
            updateUi();
            setWaitScreen(false);
        }
    };

    // Listener that's called when we finish querying the items and subscriptions we own
    protected IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
            Log.d(TAG, "Query inventory finished.");
            if (mHelper == null) return;
            if (result.isFailure()) {
                complain("Ошибка восстановления произведенных покупок: " + result);
                return;
            }
            Log.d(TAG, "Query inventory was successful.");
            premiumPurchase = inventory.getPurchase(SKU_NOADS);
            noAds = (premiumPurchase != null);
            Log.d(TAG, "User is " + (noAds ? "PREMIUM" : "NOT PREMIUM"));
            updateUi();
            setWaitScreen(false);
            Log.d(TAG, "Initial inventory query finished; enabling main UI.");
        }
    };

}