package com.kasimov.sport_diary2.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.kasimov.sport_diary2.fragment.AbstractTabFragment;
import com.kasimov.sport_diary2.fragment.MainActivityCalendarFragment;
import com.kasimov.sport_diary2.fragment.MainActivityExercisesFragment;
import com.kasimov.sport_diary2.fragment.MainActivityHistoryFragment;
import com.kasimov.sport_diary2.fragment.MainActivityProgressFragment;

import java.util.HashMap;
import java.util.Map;

public class SectionsPagerAdapter extends FragmentPagerAdapter {

    Map<Integer, AbstractTabFragment> tabMap;
    public SectionsPagerAdapter(FragmentManager fm) {
        super(fm);
        initTabs();
    }

    private void initTabs() {
        tabMap = new HashMap<>();
        tabMap.put(0, MainActivityCalendarFragment.getInstance());
        tabMap.put(1, MainActivityExercisesFragment.newInstance());
        tabMap.put(2, MainActivityHistoryFragment.getInstance());
        tabMap.put(3, MainActivityProgressFragment.getInstance());
    }

    @Override
    public Fragment getItem(int position) {
        return tabMap.get(position);
    }

    @Override
    public int getCount() {
        return tabMap.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabMap.get(position).getTitle();
    }
}