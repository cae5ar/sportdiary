package com.kasimov.sport_diary2.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.kasimov.sport_diary2.model.ExerciseType;
import com.kasimov.sport_diary2.util.SimpleCallback;
import com.kasimov.sport_diary2.view.ExerciseTypeItem;

import java.util.List;

public class ExercisesListAdapter extends BaseAdapter {

    public interface CheckDelegate {
        boolean check(ExerciseType exerciseType);
    }

    protected LayoutInflater inflater;
    protected Context context;
    protected ImageView imageView;
    protected SimpleCallback clickCallback;
    protected SimpleCallback removeCallback;
    protected CheckDelegate checkDelegate;
    protected List<ExerciseType> exercisesTypes;

    public ExercisesListAdapter(Context context, SimpleCallback clickCallback,  SimpleCallback removeCallback,CheckDelegate checkDelegate, List<ExerciseType> exercises) {
        this.context = context;
        this.clickCallback = clickCallback;
        this.removeCallback = removeCallback;
        this.checkDelegate = checkDelegate;
        this.exercisesTypes = exercises;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return exercisesTypes.size();
    }

    @Override
    public ExerciseType getItem(int i) {
        return exercisesTypes.get(i);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        final ExerciseType exerciseType = getItem(i);
        ExerciseTypeItem exerciseTypeItem  =  new ExerciseTypeItem(context, exerciseType, clickCallback, new SimpleCallback() {
            @Override
            public void callback(Object value) {
                removeCallback.callback(value);
                ExercisesListAdapter.this.notifyDataSetChanged();
            }
        }, checkDelegate);

//        RelativeLayout lastExerciseItemView = (RelativeLayout) inflater.inflate(R.layout.exercise_item, null);
//        ImageView imageView = (ImageView) lastExerciseItemView.findViewById(R.id.exerciseIcon);
//        if (exerciseType.getGroup() == ExerciseGroup.AEROBIC)
//            imageView.setImageResource(R.drawable.aero);
//        TextView textView = (TextView) lastExerciseItemView.findViewById(R.id.exerciseName);
//        textView.setText(exerciseType.getTitle());
//        if (clickCallback != null) {
//            lastExerciseItemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    clickCallback.callback(exerciseType);
//                }
//            });
//        }
//        ImageView exerciseRemoveIcon = (ImageView) lastExerciseItemView.findViewById(R.id.exerciseRemoveIcon);
//        if (removeCallback != null){
//            exerciseRemoveIcon.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    removeCallback.callback(exerciseType);
//                    ExercisesListAdapter.this.notifyDataSetChanged();
//                }
//            });
//        }
//        if(checkDelegate != null){
//            exerciseRemoveIcon.setVisibility(checkDelegate.check(exerciseType) ? View.VISIBLE : View.GONE);
//        }
//        if (exerciseType.getVideoName() != null) {
//            ImageView exerciseVideoIcon = (ImageView) lastExerciseItemView.findViewById(R.id.exerciseVideoIcon);
//            exerciseVideoIcon.setVisibility(View.VISIBLE);
//            exerciseVideoIcon.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent intent = new Intent(context, VideoPlayerActivity.class);
//                    intent.putExtra(VIDEO_NAME, exerciseType.getVideoName());
//                    context.startActivity(intent);
//                }
//            });
//        }
//        lastExerciseItemView.setBackgroundResource(R.drawable.item_panel_top_bottom_border);

        return exerciseTypeItem;
    }
}
