package com.kasimov.sport_diary2.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kasimov.sport_diary2.R;
import com.kasimov.sport_diary2.model.Exercise;
import com.kasimov.sport_diary2.model.ExerciseGroup;

public class BelowCalendarExercisesAdapter extends BaseAdapter{

    protected LayoutInflater inflater;
    private Exercise [] exercises;
    protected Context context;

    public BelowCalendarExercisesAdapter(Context context, Exercise ... exercises) {
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.exercises = exercises;
    }

    @Override
    public int getCount() {
        return exercises.length;
    }

    @Override
    public Exercise getItem(int i) {
        return exercises[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Exercise exercise = getItem(i);
//        TextView name = new TextView(context);
//        TextView value = new TextView(context);

        LinearLayout lineaLayout = (LinearLayout) inflater.inflate(R.layout.exercise_item_with_value, null);
        TextView name = (TextView)lineaLayout.findViewById(R.id.exerciseName);
        TextView value = (TextView)lineaLayout.findViewById(R.id.exerciseValue);
        name.setText(exercise.getType().getTitle());
        if(exercise.getType().getGroup()==ExerciseGroup.AEROBIC) {
            String s = new String("Скорость: ");
            s += exercise.getSpeed();
            s += " ";
            s += exercise.getSpeedUnit().title;
            s += ", длительность: ";
            s += exercise.getDuration();
            s += "мин.";
            value.setText(s);
        }else{
            String s = new String("Рабочий вес: ");
            s += exercise.getWorkingWeight();
            s += " ";
            s += exercise.getWeightUnit().title;
            s += ", подходов: ";
            s += exercise.getApproachesNumber();
            s += ".";
            value.setText(s);
        }
//        TextView exerciseGroupName = (TextView) exerciseGroupView.findViewById(R.id.exerciseGroupName);
//        exerciseGroupName.setText(exerciseGroup.title);
//        for(final ExerciseType exerciseType:ExerciseType.getExercisesByGroup(exerciseGroup)){
//            RelativeLayout exerciseItemView = (RelativeLayout) inflater.inflate(R.layout.exercise_item, null);
//            TextView textView = (TextView) exerciseItemView.findViewById(R.id.exerciseName);
//            textView.setText(exerciseType.getTitle());
//            exerciseGroupView.addView(exerciseItemView);
//            exerciseItemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    clickCallback.callback(exerciseType);
//                }
//            });
//        }
        return lineaLayout;
    }
}
