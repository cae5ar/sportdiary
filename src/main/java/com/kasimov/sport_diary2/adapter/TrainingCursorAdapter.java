package com.kasimov.sport_diary2.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kasimov.sport_diary2.R;
import com.kasimov.sport_diary2.model.Exercise;
import com.kasimov.sport_diary2.model.Training;
import com.kasimov.sport_diary2.util.AppDatabase;
import com.kasimov.sport_diary2.view.ExerciseHistoryItem;


public class TrainingCursorAdapter extends CursorAdapter {

    private final AppDatabase appDatabase;

    public TrainingCursorAdapter(Context context, Cursor cursor) {
        super(context, cursor, 0);
        appDatabase = AppDatabase.getInstance(context);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.training_history_item, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        LinearLayout exercisesHistoryContainer = (LinearLayout) view.findViewById(R.id.exercisesHistoryContainer);
        exercisesHistoryContainer.removeAllViews();
        TextView trainingDate = (TextView) view.findViewById(R.id.trainingDate);
        Training training = appDatabase.extractTraining(cursor, new Training());
        trainingDate.setText(DateFormat.format("dd.MM.yyyy",training.getDate()));
        ExerciseHistoryItem lastHistoryItem = null;
        for(Exercise exercise: training.getExercises().values()){
            if(exercise.getDelta()!=null){
                lastHistoryItem = new ExerciseHistoryItem(context, exercise, training.getType());
                exercisesHistoryContainer.addView(lastHistoryItem);
            }
        }
        lastHistoryItem.getRootView().setBackgroundResource(0);
    }
}