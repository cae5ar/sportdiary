package com.kasimov.sport_diary2.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kasimov.sport_diary2.R;

import java.util.List;

// Custom Adapter for Spinner
public class CustomSpinnerAdapter extends ArrayAdapter<String> {

    private Context context1;
    private List<String> data;
    public Resources res;
    LayoutInflater inflater;

    public CustomSpinnerAdapter(Context context, List<String> objects) {
        super(context, R.layout.spinner_row, objects);
        context1 = context;
        data = objects;
        inflater = (LayoutInflater) context1.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {
        RelativeLayout row = (RelativeLayout) inflater.inflate(R.layout.spinner_row, parent, false);
        TextView tvCategory = (TextView) row.getChildAt(0);
        tvCategory.setText(data.get(position).toString());
        return row;
    }
}