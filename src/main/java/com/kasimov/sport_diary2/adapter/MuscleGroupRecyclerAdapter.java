package com.kasimov.sport_diary2.adapter;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.aakira.expandablelayout.ExpandableLayout;
import com.github.aakira.expandablelayout.ExpandableLayoutListenerAdapter;
import com.github.aakira.expandablelayout.ExpandableLinearLayout;
import com.github.aakira.expandablelayout.Utils;
import com.kasimov.sport_diary2.R;
import com.kasimov.sport_diary2.model.ExerciseGroup;
import com.kasimov.sport_diary2.model.ExerciseType;
import com.kasimov.sport_diary2.util.SimpleCallback;
import com.kasimov.sport_diary2.view.ExerciseTypeItem;

import java.util.List;

public class MuscleGroupRecyclerAdapter extends RecyclerView.Adapter<MuscleGroupRecyclerAdapter.ViewHolder> {

    private final SimpleCallback clickCallback;
    private final SimpleCallback removeCallback;
    private final ExercisesListAdapter.CheckDelegate checker;
    private final List<ExerciseGroup> data;
    private Context context;
    private SparseBooleanArray expandState = new SparseBooleanArray();

    public MuscleGroupRecyclerAdapter(SimpleCallback clickCallback, SimpleCallback removeCallback, ExercisesListAdapter.CheckDelegate checker, final List<ExerciseGroup> data) {
        this.clickCallback = clickCallback;
        this.removeCallback = removeCallback;
        this.checker = checker;
        this.data = data;
        for (int i = 0; i < data.size(); i++) {
            expandState.append(i, false);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        this.context = parent.getContext();
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.muscle_group_row, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        ExerciseGroup item = data.get(position);
        holder.setIsRecyclable(false);
        holder.textView.setText(item.title);
        holder.expandableLayout.setInRecyclerView(true);
        holder.expandableLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.md_white_1000));
        holder.expandableLayout.setInterpolator(Utils.createInterpolator(Utils.FAST_OUT_SLOW_IN_INTERPOLATOR));
        holder.expandableLayout.setExpanded(expandState.get(position));
        holder.expandableLayout.setListener(new ExpandableLayoutListenerAdapter() {
            @Override
            public void onPreOpen() {
                createRotateAnimator(holder.buttonLayout, 0f, 180f).start();
                expandState.put(position, true);
            }

            @Override
            public void onPreClose() {
                createRotateAnimator(holder.buttonLayout, 180f, 0f).start();
                expandState.put(position, false);
            }
        });
        holder.buttonLayout.setRotation(expandState.get(position) ? 180f : 0f);
        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                onClickButton(holder.expandableLayout);
            }
        });
        holder.buttonLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                onClickButton(holder.expandableLayout);
            }
        });
        ExerciseTypeItem typeItem = null;
        for(ExerciseType type :ExerciseType.getExercisesByGroup(item)){
            typeItem = new ExerciseTypeItem(context,type, clickCallback, removeCallback, checker);
            holder.exerciseView.addView(typeItem);
        }
        typeItem.setItemBackground(R.drawable.item_panel_top_bottom_border);
    }

    private void onClickButton(final ExpandableLayout expandableLayout) {
        expandableLayout.toggle();
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textView;
        public RelativeLayout buttonLayout;
        public ExpandableLinearLayout expandableLayout;
        public LinearLayout exerciseView;

        public ViewHolder(View v) {
            super(v);
            textView = (TextView) v.findViewById(R.id.muscleGroupText);
            buttonLayout = (RelativeLayout) v.findViewById(R.id.button);
            expandableLayout = (ExpandableLinearLayout) v.findViewById(R.id.expandableLayout);
            exerciseView = (LinearLayout) v.findViewById(R.id.exercise_list);
        }
    }

    public ObjectAnimator createRotateAnimator(final View target, final float from, final float to) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(target, "rotation", from, to);
        animator.setDuration(200);
        animator.setInterpolator(Utils.createInterpolator(Utils.FAST_OUT_SLOW_IN_INTERPOLATOR));
        return animator;
    }
}