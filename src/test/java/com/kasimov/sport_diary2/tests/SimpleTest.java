package com.kasimov.sport_diary2.tests;

import android.content.Context;

import com.kasimov.sport_diary2.util.DateUtils;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.text.DecimalFormat;
import java.util.Date;

@RunWith(MockitoJUnitRunner.class)
public class SimpleTest {

    @Mock
    Context mMockContext;

    @Test
    public void getDate(){
        System.out.println("ExampleUnitTest" + DateUtils.trimTime(new Date().getTime()).getTimeInMillis()+"");

        System.out.println(new DecimalFormat("#.##").format(1.199));
        System.out.println(new DecimalFormat("+#.##").format(-3));
        System.out.println(String.format("%5.2f ",5.0));
    }

}
